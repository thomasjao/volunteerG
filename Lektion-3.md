## Wortschatz
### Verben

|   |   |   |   |
|---|---|---|---|
| backen | erkennen | kennen | schmecken |
| bestellen | erz&auml;hlen | kochen | trinken |
| bezahlen | essen | m&ouml;gen | &uuml;ben |
| brauchen | galuben | nehmen |

### Nomen

|   |   |   |   |
|---|---|---|---|
| s Abendessen | e Frucht, "-e | r Liter, - | r Schinken, - |
|   | s Fr&uuml;hst&uuml;ck | r L&ouml;ffel, - | r Schnaps, "-e |
| r Alkohol | e Gabel, -n | e Mark | e Schokolade, -n |
| e Anzeige, -n | r Gasthof, "-e | e Marmelade, -n | e So&szlig;e, -n |
| r Apfel, "- | s Gem&uuml;se |   | e Speisekarte, -n |
| s Bier | s Gericht, -e | s Mehl | s Steak, -s |
|   | s Gespr&auml;ch, -e | s Messer, - | e Suppe, -n |
| e Bohne, -n | s Getr&auml;nk, -e | e Milch | e Tasse, -n |
| s Brot, -e | s Gew&uuml;rz, -e | s Mineralwasser | r Tee, -s |
| s Br&ouml;tchen, - | s Glas, "-er |   | r Teller, - |
| e Butter | s Gramm | r Nachtisch, -e | e Tomate, -n |
| e Dose, -n | s H&auml;hnchen | s &Ouml;l, -e | e Vorspeise, -n |
| s Ei, -er | r Kaffee | r Pfeffer | e W&auml;sche |
| s Eis | e Kartoffel, -n | s Pfund | e Wurst, "-e |
| e Erdbeere, -n |   | r Preis, -e | r Zettel, - |
| r Export | r K&auml;se | r Reis | r Zucker |
| r Fisch, -e | s Kilo, -s | r Rotwein, -e | e Zwiebel, -n |
| e Flasche, -n | s Kotelett, -s | r Saft, "-e |   |
| s Fleisch | r Kuchen, - | e Sahne |   |
| e Frage, -n | e Limonade, -n | r Salat, -e |   |

### Adjektive

|   |   |   |   |
|---|---|---|---|
| billig | gr&uuml;n | nah | schlank |
| bitter | hart | normal | stark |
| dunkel | hell | phantastisch | s&uuml;&szlig; |
| eng | hoch | rot | trocken |
| fett | kalt | salzig | typisch |
| frisch | lieber | sauer | warm |
| gro&szlig; | mild | scharf | wichtig |

### Adverbien

|   |   |   |   |
|---|---|---|---|
| abends | gern | nur | verschieden |
| besonders | lieber | oben | vor allem |
| danach | manchmal | oft | vorwiegend |
| dann | mittags | so | zuerst |
| fast | morgens | sofort | zusammen |
| ganz | nachmittags | &uuml;berall |   |
| genug | nat&uuml;rlich | unten |   |

### Funktionsw&ouml;rter

|   |   |   |   |
|---|---|---|---|
| alle | etwas | pro | zu |
| als | jeder | viel |   |
| doch | mit | welcher? |   |

### Ausdr&uuml;cke

es gibt &nbsp; &nbsp; vor allem

### Abk&uuml;rzungen

g s Gramm &nbsp; &nbsp; kg s Kilogramm

## Grammatik
### Definiter Artikel im Akkusativ

|   |   |   |   |   |   |
|---|---|---|---|---|---|
| *Maskulinum*<br />*Femininum*<br />*Neutrum* | *Singular:* | **den** Stuhl<br />die Lampe<br />das Klavier | *Plural:* | die St&uuml;hle<br /> &nbsp; &nbsp; Lampen<br /> &nbsp; &nbsp; Klaviere |


### Idenfiniter Artikel, Possessivartikel, Negation im Akkusativ

|   |   |   |   |   |
|---|---|---|---|---|
| *Singular* | *Idenfiniter Artikel*<br />einen Stuhl<br />eine Lampe<br />ein Regal | *Possessivartikel*<br />meinen/seinen<br />deinen/Ihren | Stuhl | *Negation* |
|   |   | meine / seine | Lampe | keine Lampe |
|   |   | mein / sein<br />dein / Ihr | Regal | kein Regal |
| *Plural:* | St&uuml;hle<br />Lampen<br />Regale | meine<br />deine / Ihre | St&uuml;hle<br />Lampen<br />Regale | keine | St&uuml;hle<br />Lampen<br />Regale |

### Imperativ

*Nimm* doch noch etwas Fleisch, *Christian*!

*Nehmen Sie* doch noch etwas Fleisch, *Frau Herzog*!
