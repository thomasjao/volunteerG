- [ ] **abridge**  縮短；刪節  <kbd>sym</kbd>shorten, condense, compress, lessen, cut down, abbreviate<br/>
     _It was **abridged** from the original work._
- [ ] **absolve**  赦免；解除 (責任) 等  <kbd>sym</kbd> relieve, forgive, clear, release, loose, pardon, set free<br/>
_I was **absolved** of blame in the matter._
- [ ] **absorb**  吸收；併吞  <kbd>sym</kbd> take in, suck in, assimilate, swallow up<br/>
_A blotter **absorbs** ink._
- [ ] **abstain**  戒絕  <kbd>sym</kbd> refrain, forbear, restrain<br/>
_Athletes usually **abstain** from smoking._
- [ ] **abstract**  摘要；提煉  <kbd>sym</kbd> make an abstract of, take out of, summerize, extract<br/>
_The student is asked to **abstract** his lengthy report._
- [ ] **accelerate**  加速進行；迫使  <kbd>sym</kbd> hasten, quicken, push forward, urge on<br/>
_The enactment of the bill **accelerated** the fall of the government._
- [ ] **accommodate**  供應；容納  <kbd>sym</kbd> serve, supply, furnish, hold<br/>
_This big room will **accommodate** six beds._
- [ ] **accompany**  伴；隨  <kbd>sym</kbd> go with, attend, escort, follow, keep company with<br/>
_Please **accompany** me on my walk._
- [ ] **accost**  向人搭話  <kbd>sym</kbd> come alongside, address, speak to<br/>
_I was **accosted** by a beggar._
- [ ] **accredit**  歸功於... <kbd>sym</kbd> attribute, give credit to<br/>
_We **accredit** the invention of the telephone to Bell._
- [ ] **accumulate**  積聚 <kbd>sym</kbd> pile, amass, aggregate, collect, store, grow <kbd>N.</kbd> accumulation<br/>
_He **accumulated** great wealth by hard work and wise investment._
- [ ] **accuse**  控告；歸咎 <kbd>sym</kbd> blame, charge, incriminate  <kbd>N.</kbd> accusation<br/>
_He was **accused** of stealing a typewriter._
- [ ] **acquit**  宣告無罪 <kbd>sym</kbd> discharge, release, absolve, excuse<br/>
_The jury **acquitted** her even though she was guilty._
- [ ] **adhere**  黏著；堅持  <kbd>sym</kbd> stick, cling, hold, cleave <kbd>N.</kbd> adherence <kbd>adj.</kbd> adherent<br/>
_It had rained all day and the mud **adhered** to our shoes._
- [ ] **adjust**  調節；使適於 <kbd>sym</kbd> arrange, rectify, settle, fit, suit <kbd>N.</kbd> adjustment<br/>
_These desks and seats can be **adjusted** to the height of any child._
- [ ] **administer**  管理 <kbd>sym</kbd> direct, manage, conduct<br/>
_We need the best men to **administer** the affairs of the state._
- [ ] **agonize**  使受苦；使苦悶 <kbd>sym</kbd> torment, torture, rack, distress, be tormented <kbd>N.</kbd> agony<br/>
_All the time they **agonized** and prayed._
- [ ] **alienate**  使疏遠；轉讓 (房地產等) <kbd>sym</kbd> estrange, wean, disaffect, assign <kbd>N.</kbd> alienation<br/>
_She was **alienated** from her friend by his foolish behavior._
- [ ] **alight**  降下；落在 <kbd>sym</kbd> descend, dismount, get down, stop, rest<br/>
_The bird **alighted** on a twig._
- [ ] **allocate**  分配；定位置 <kbd>sym</kbd> allot, locate, assign<br/>
_Bigger rations were **allocated** to those people._
- [ ] **allure**  引誘 <kbd>sym</kbd> entice, tempt, seduce, beguile, attract lure, coax lure, coax, invite <kbd>adj.</kbd>alluring <kbd>N.</kbd> allurement<br/>
_Don't be **allured** into bars by the pretty waitresses._
- [ ] **alternate**  輪流 (常與 with 連用) <kbd>sym</kbd> reciprocate, act interchangeably, rotate<br/>
_Wet days **alternated** with fine days._
- [ ] **ambush**  埋伏  <kbd>sym</kbd> waylay<br/>
_We were **ambushed**._
- [ ] **annex**  附加 <kbd>sym</kbd> join, attach, add, append, affix, tag<br/>
_Happiness is not always **annexxed** to wealth._
- [ ] **annihilate**  消滅 <kbd>sym</kbd> destroy, extinguish, quench, exterminate, nullify, extirpate, put and end to<br/>
_Napoleon's fleet was **annihilated** by Nelson._
- [ ] **annotate**  註解 <kbd>sym</kbd> make notes on, make comments on, comment on, explain, illustrate<br/>
_Some people **annotate** as they read._
- [ ] **apologize**  道歉 <kbd>sym</kbd> offer an excuse, amke an apology<br/>
_She **apologized** to her teacher for coming to school late._
- [ ] **apostatize** 背教；脫黨 <kbd>sym</kbd> become an apostate<br/>
_He **apostatized** from his party._
- [ ] **appall**  使驚駭 <kbd>sym</kbd> frighten, dismay, daunt, terrify, horrify, put in great fear<br/>
_They were **appalled** at the news._
- [ ] **arraign**  控告 <kbd>sym</kbd> prosecute, accuse, charge, denounce, indict, criminate, censure<br/>
_He is **arraigned** for high treason._
- [ ] **array**  排列 <kbd>sym</kbd> arrange, rank, set in order, set in line<br/>
_The general **arrayed** his troops for the battle._
- [ ] **asperse**  誹謗 <kbd>sym</kbd> vivify, slander, traduce, defame<br/>
_He said law enforcement facts turn the tide upon those who seek to **asperse** the country's good name._
- [ ] **aspire**  熱望 <kbd>sym</kbd> desire, yearn, long<br/>
- [ ] **assemble**  聚集 <kbd>gather, collect, convene, connect, put together<br/>
_The students were **assembled** in the school hall._
- [ ] **assimilate**  吸收 <kbd>sym</kbd> make alike, absorb <kbd>ant</kbd> dissimilate<br/>
_We **assimilate** some kinds of food more easily than others._
- [ ] **assuage**  緩和 <kbd>sym</kbd> moderate, mitigate, soothe, pacify<br/>
_Nothing can **assuage** the widow's grief._
- [ ] **assure**  確告 <kbd>sym</kbd> make certain, make sure<br/>
_I **assure** you there is no danger here._
- [ ] **avouch**  確說；斷言 <kbd>sym</kbd> assert, declare, protect, allege, maintain, affirm<br/>
_Millions were ready to **avouch** the exact contrary._
- [ ] **avow**  公開；承認 <kbd>sym</kbd> confess, acknowledge, admit, own<br/>
_He **avowed** that he could not sing._
- [ ] **babble**  嘮叨；說話不清 <kbd>sym</kbd> chat, gossip, tattle, prattle, gibber<br/>
_The sick man **babbles** of home._
- [ ] **backbite**  背後誹謗 <kbd>sym</kbd> defame, malign, traduce, revile, libel<br/>
_His classmates don't enjoy his company because he always **backbites** others._
- [ ] **badger**  困擾 <kbd>sym</kbd> persecute, worry, annoy, vex, tease, torment<br/>
_Tom has been **badgering** his big brother to buy him a camera._
- [ ] **bask**  取暖；曝日；沐浴於 <kbd>sym</kbd> lie warming, rejoice, be bathed, prosper<br/>
_The cat **basks** before the fire._
- [ ] **baste**  毆打；公開責罵 <kbd>sym</kbd> beat, cane, thrash, cudgel<br/>
_The paper **basted** the candidate for irresponsible statement._
- [ ] **bedeck**  裝飾 <kbd>sym</kbd> adorn, embellish, decorate, ornament, beautify, garnish<br/>
_The duchess **bedecked** herself with jewels._
- [ ] **beg**  懇求 <kbd>sym</kbd> implore, crave, ask, pray for, petition for<br/>
_The prisoner **begged** the judge for mercy._
- [ ] **beget**  引起；產生 <kbd>sym</kbd> produce, breed, generate, bring about<br/>
_War **begets** misery and ruin._
- [ ] **begrudge**  羨慕;嫉妒 <kbd>sym</kbd> grudge, envy<br/>
_No one **begrudged** him his good fortune._
- [ ] **beguile**  欺騙；逍遣 <kbd>sym</kbd> delude, deceive, amuse, cheer<br/>
_He **beguiled** me into signing this contract._
- [ ] **belie**  掩飾 <kbd>sym</kbd> falsify, slander, defame, contradict<br/>
_His cheerful appearance **belied** his feelings._
- [ ] **bereave**  剝奪 <kbd>sym</kbd> deprive, rob, take away from<br/>
_Nothing can **bereave** us of such sweet memories._
- [ ] **beset**  包圍 <kbd>sym</kbd>  surround, encircle, enclose, encompass<br/>
_The ship was **beset** with ice._
- [ ] **besiege**  圍 <kbd>sym</kbd> encompass, beset, surroung<br/>
_This city was **besieged** by the enemy for two years._
- [ ] **bespeak**  指出 <kbd>sym</kbd> order, speak to indicate, imply<br/>
_Today's events **bespeak** future tragedy._
- [ ] **betoken**  表示 <kbd>sym</kbd> signify, indicate, represent, denote<br/>
_His smile **betokens** his satisfaction._
- [ ] **bluff**  虛張聲勢 <kbd>sym</kbd> deceive, threaten<br/>
_He is **bluffing**._
- [ ] **brace**  支持；使固定 <kbd>sym</kbd> strengthen, reinforce, support, tighten, give strength to<br/>
_He **braces** himself against a wall._
- [ ] **brandish**  揮 <kbd>sym</kbd> wave, flourish<br/>
_The knight drew his sword and **brandished** it at his enemy._
- [ ] **brawl**  爭吵 <kbd>sym</kbd> quarrel, dispute, wrangle, roar<br/>
_Statemen of the nation were **brawling** with each other._
- [ ] **brew**  釀造；圖謀  <kbd>sym</kbd> produce by fermentation, ferment, contrive, devise, plot<br/>
_He **brewed** beer for home use._
- [ ] **bruise**  打傷 <kbd>sym</kbd> crush, squeeze, break, batter, deface<br/>
_He **bruised** his finger with a hammer._
- [ ] **butt**  撞 <kbd>sym</kbd> strike, push<br/>
_The goat **butts**._
- [ ] **cackle**  呵呵笑 <kbd>sym</kbd> laugh, giggle, snicker<br/>
_After each joke the old man **cackled** enjoyment._
- [ ] **capsize**  傾覆 <kbd>sym</kbd> overturn, upset<br/>
_The boat **capsized** in the midst of a whirlpool._
- [ ] **captivate**  迷惑 <kbd>sym</kbd> fascinate, charm, enchant, bewitch, enamor<br/>
_He was **captivated** by her beauty._
- [ ] **caress**  撫愛 <kbd>sym</kbd> pet, fondle, dandle<br/>
_The child was **caressed** by his mother with her hand._
- [ ] **castigate**  譴責 <kbd>sym</kbd> upbraid, fall foul of, chastise, whip, criticize<br/>
_The principal **castigated** the students who had insulted their teacher._
- [ ] **chafe**  擦熱；煩擾 <kbd>sym</kbd> rub, weat by rubbing, vex, irritate, provoke, annoy, fret, chagrin<br/>
_She **chafed** her cold hands._
- [ ] **cherish**  撫育 <kbd>sym</kbd> nurture, foster, nurse, nourish, sustain<br/>
_A mother **cherishes** her baby._
- [ ] **chide**  叱責 <kbd>sym</> rebuke, censure, reprove, reprimand, fret<br/>
_His father **chided** him for being late._
- [ ] **chirp**  吱喳鳴出或叫出 <kbd>sym</kbd> peep, chirrup, twitter, cheep<br/>
_The boy **chirps** a few notes._
- [ ] **choke**  使窒息 <kbd>sym</kbd> suffocate, smother, stifle, suppress<br/>
_The smoke almost **choked** me._
- [ ] **chop**  砍 <kbd>sym</kbd> cut, mince, fell, hew<br/>
_He was **chopping** wood._
- [ ] **chuckle**  咯咯而笑 <kbd>sym</kbd> tittle, giggle, laugh<br/>
_He was **chuckling** to himself over what he was reading._
- [ ] **circumscribe**  劃界限；限制 <kbd>sym</kbd> confine, limit, encircle, bound, surround, restrict<br/>
_The prisoner's activities were **circumscribed**._
- [ ] **circumvent**  欺詐；以計勝過 <kbd>sym</kbd> outfox, baffle, deceive, trick<br/>
_The crook tried to **circumvent** the little girl._
- [ ] **clamor**  喧鬧；大聲叫嚷 <kbd>sym</kbd> cry out, vociferate, shout<br/>
_The speaker was **clamored** down by the audience._
- [ ] **cleave**  黏著；忠守 <kbd>adhere, cohere, stick, cling<br/>
_That stubborn man always **cleaved** to his ideas._
- [ ] **clinch**  釘牢；揪住 <kbd>sym</kbd> grasp, grip, clutch, fasten, fix<br/>
_When the boxers **clinched**, the crowd hissed._
- [ ] **clip**  修剪；包圍 <kbd>sym</kbd> shear, encircle, encompass<br/>
_Our dog is **clipped** every summer._
- [ ] **clog**  阻礙 <kbd>sym</kbd> shackle, hamper, trammel, obstruct, hinder, impede<br/>
_The machinery was **clogged** with thick oil and dirt._
- [ ] **clutch**  抓緊 <kbd>sym</kbd> grip, grasp, grab, seize, grabble, clasp, catch, snatch, clench<br/>
_He **clutched** at the rope we threw to him._
- [ ] **coddle**  嬌養；溺愛 <kbd>sym</kbd> nurse, caress, pet<br/>
_She **coddled** her son when he was sick._
- [ ] **cog**  欺騙 <kbd>sym</kbd> wheedle, coax, deceive<br/>
_The swindler **cogged** dice._
- [ ] **cohere**  附著；連貫 <kbd>sym</kbd> adher to each other, fit with, agree, stick together<br/>
_A sentence that does not **cohere** is hard to understand._
- [ ] **coin**  鑄 (幣)；創造 <kbd>sym</kbd> invent, devise, create, mould<br/>
_He is **coining** money in his new business._
- [ ] **collate**  對照；校勘 <kbd>sym</kbd> compare, estimate, examine critically<br/>
_If you **collate** the latter with the earlier edition, you will find that many parts have been rewritten._
- [ ] **commingle**  混合 <kbd>sym</kbd> mingle, blend, mix, combine<br/>
_Fact is inextricably **commingled** with fiction._
- [ ] **commiserate**  憐憫；同情 <kbd>sym</kbd> compassionate, have pity, feel for, feel sorry for, express sympathy for<br/>
_You'd better **commiserate** John on his recent misfortunes._
- [ ] **comply**  順從；應允 <kbd>sym</kbd> observe, fulfill, satsify, complete, be faithful to, adhere to<br/>
_We should **comply** with the doctor's request._
- [ ] **comport**  相稱 <kbd>sym</kbd> agree, harmonize, accord, behave<br/>
_His conduct did not **comport** with his high position._
- [ ] **comprise**  包括 <kbd>sym</kbd> comprehend, involve, embrace, contain, include<br/>
_His course of study **comprises** English, Chinese, mathematics, physics and chemistry._
- [ ] **conceive**  想像 <kbd>sym</kbd> imagine, think, comprehend, suppose<br/>
_I **conceived** that there must be some difficulties._
- [ ] **concoct**  編造；虛構 <kbd>sym</kbd> devise, plan, plot, contrive, invent<br/>
_He **concocted** an excuse for being late._
- [ ] **concur**  同意 <kbd>sym</kbd> agree, approve, combine<br/>
_The judge all **concurred** in giving John the prize._
- [ ] **condone**  寬恕；赦免 <kbd>sym</kbd> forgive, pardon, overlook, excuse<br/>
_I cannot **condone** such a crime._
- [ ] **confederate**  聯合；同盟 <kbd>sym</kbd> league, ally, unite<br/>
_The tribes **confederated** in the war against intruders._
- [ ] **congregate**  聚集 <kbd>sym</kbd> assemble, collect, gather, convene<br/>
_Students are **congregated** in the auditorium._
- [ ] **conjure**  懇求 <kbd>sym</kbd> beseech, pray, entreat, supplicate, invoke, charm, enchant<br/>
_I **conjure** you not to betray your country._
- [ ] **consecrate**  泰為神聖；供獻 <kbd>sym</kbd> sanctify, dedicate, devote, ordain<br/>
_The new church was **consecrated** by the Bishop._
- [ ] **consign**  移交 <kbd>sym</kbd> deliver, transfer, hand over, commit, entrust<br/>
_They **consigned** him to jail._
- [ ] **consort**  結交 <kbd>sym</kbd> associate with, keep company with<br/>
_Do not **consort** with thieves._
- [ ] **conspire**  陰謀；密謀 <kbd>sym</kbd> plot, scheme, intrigue<br/>
_They **conspired** against the government._
- [ ] **construe**  解釋；翻譯 <kbd>sym</kbd> interpret, explain, translate, render<br/>
_His remarks were wrongly **construed**._
- [ ] **contaminate**  污損 <kbd>sym</kbd> pollute, corrupt, defile, stain, infect<br/>
_Flies **contaminate** milk._
- [ ] **contemn**  蔑視 <kbd>sym</kbd> disdain, scorn, despise<br/>
_The wicked **contemn** God._
- [ ] **contemplate**  凝視；沈思 <kbd>sym</kbd> gaze upon, ponder, think about, muse on, consider<br/>
_All day he did nothing but **contemplate** his plan._
- [ ] **contradict**  反駁；抵觸 <kbd>sym</kbd>oppose, counteract, belie, thwart<br/>
_He **contradicted** me openly._
- [ ] **controvert**  駁擊；辯論 <kbd>sym</kbd> contest, dispute, debate, canvass<br/>
_The statement of the last witness **controverts** the evidence of the first two._
- [ ] **convene**  集合 <kbd>sym</kbd> assemble, meet, muster, come together, convoke, summon<br/>
_The next meeting will **convene** on the coming Sunday at the same place._
- [ ] **convert**  改變；變更 <kbd>sym</kbd> change, transmute, transform<br/>
_They **converted** the study into a nursery when the baby was born._
- [ ] **convoke**  召集 <kbd>sym</kbd> convene, assemble, summon, call together<br/>
_The king **convoked** Parliament to cope with the impending danger._
- [ ] **convoy**  護送 <kbd>sym</kbd> accompany, escort, keep company with, attend<br/>
_The ships were **convoyed** into the harbor._
- [ ] **coop**  拘禁 <kbd>sym</kbd> cage, confine, imprison, shut up<br/>
_One feels **cooped** up in a cabin on board a ship._
- [ ] **cower**  畏縮 <kbd>sym</kbd> crouch, cringe, shrink quivering<br/>
_The dog **cowered** under the table when his master raised the whip._
- [ ] **cram** 填塞；倉卒用功 <kbd>sym</kbd> stuff, fill, full, prepare for examination<br/>
_He **crammed** all his clothes into the bag._
- [ ] **crash**  撞碎 <kbd>sym</kbd> shatter, smash, shiver, crack<br/>
_The dishes **crashed** to the floor._
- [ ] **craze**  使發狂 <kbd>sym</kbd> madden, make insane, derange<br/>
_They were **crazed** by the famine and pestilence of that bitter winter._
- [ ] **crumple**  壓皺 <kbd>sym</kbd> wrinkle, rumple<br/>
_He **crumpled** the letter into a ball._
- [ ] **curtail**  減縮 <kbd>sym</kbd> cut short, shorten, abridge<br/>
_John's father **curtailed** his allowance._
- [ ] **dally**  荒癈 (時日) <kbd>sym</kbd> trifle, lose time, wate time, dawdle, fritter<br/>
_James **dallied** away the afternoon._
- [ ] **dangle**  懸擺 <kbd>sym</kbd> hang loose, swing, fawn, hang about<br/>
_The leaves **dangled** in the wind._
- [ ] **dart**  投擲 <kbd>sym</kbd> hurl, throw, let fly, launch<br/>
_He **darted** a dagger at his enemy._
- [ ] **dawdle**  閒蕩 <kbd>sym</kbd> trifle, fiddle, dally, quiddle, waste time, fool away time, fritter away time, potter<br/>
_Stop **dawdling** and do something useful!_
- [ ] **dazzle**  使目眩；使迷惑 <kbd>sym</kbd> astonish, surprise, confound, overpower<br/>
_He was **dazzled** by her beauty._
- [ ] **debauch**  使誤入岐途；敗壞 <kbd>sym</kbd> corrupt, deprave, desuce, vitiate<br/>
_Bad companions had **debauched** the boy._
- [ ] **debilitate**  使衰弱 <kbd>sym</kbd> weaken, enfeeble, exhaust, prostrate, enervate <kbd>ant</kbd> strengthen<br/>
_He was **debilitated** by excesses._
- [ ] **deery**  非難；譴責 <kbd>sym</kbd> depreciate, disparage, condemn, cry down, traduce, discredit<br/>
_The mayor **deeried** gambling in all its forms._
- [ ] **dedicate** <kbd>sym</kbd> hallow, devote, consentrate, sanctify<br/>
_This shrine is **dedicated** to the memory of soldiers killed in the defence of the country._
- [ ] **deface**  傷毀 (外表或美觀) <kbd>sym</kbd> mutilate, disfigure, scratch, deform, spoil, blotch, soil<br/>
_Thoughtless boys have **defaced** the desks by marking on them._
- [ ] **defame**  誹謗；損毀名譽 <kbd>sym</kbd> slander, malign, traduce, abuse, blemish, vilify, revile, asperse<br/>
_Men in public life are sometimes **defamed** by opponents._
- [ ] **defile**  弄汅 <kbd>sym</kbd> soil, stain, tarnish, make foul, sully, pollute, corrupt, seduce<br/>
_Rivers are often **defiled** by waste from factories._
- [ ] **deflect**  使偏離 <kbd>sym</kbd> diverge, deviate, swerve, turn aside<br/>
_The bullet struck a wall and was **deflected** from its course._
- [ ] **deform**  使變形 <kbd>sym</kbd> distort, disfigure, deface, make ugly<br/>
_Shoes that are too tight **deform**  the feet._
- [ ] **defraud**  欺詐 <kbd>sym</kbd> deceive, cheat, cozen, dupe, delude, beguile<br/>
_That man was **defrauded** of his estate._
- [ ] **degenerate**  退步；衰敗 <kbd>sym</kbd> decline, sink, decay, deteriorate, become worse, retrograde<br/>
_Are the young people of today **degenerating**?_
- [ ] **degrade**  使墮落；降級 <kbd>sym</kbd> disgrace, discredit, abase, dishonor, depose<br/>
_The captain was **degraded** for disobeying orders._
- [ ] **delude**  欺騙；迷惑 <kbd>sym</kbd> beguile, mislead, deceive, cozen, gull, trick, cheat, dupe, impose, opon<br/>
_She **deluded** herself with empty dreams._
- [ ] **demur**  躊躇；抗議 <kbd>sym</kbd> stop, hesitate, waver, pause, raise objection<br/>
_She **demurred** at the difficulty._
- [ ] **depict**  描寫；敘述 <kbd>sym</kbd> sketch, picture, describe, portray<br/>
_Biblical scenes had been **depicted** in the tapestry._
- [ ] **deplore**  悲痛；深悔 <kbd>sym</kbd> bewail, mourn, grieve for, bemoun, sorrow over<br/>
_He **deplored** the death of a close friend._
- [ ] **depose**  革職；癈 (王位) <kbd>sym</kbd> dismiss, dethrone, oust, displace, degrade, reduce<br/>
_The king was **deposed** by the revolution._
- [ ] **deprecate**  祈禱以求免於 (災禍等) <kbd>sym</kbd> view with regret<br/>
_Shall we prostrate ourselves and **deprecate** his wrath?_
- [ ] **derange**  使錯亂 <kbd>sym</kbd> confuse, disorder, disarrange, displace, unsettle, disturb, madden<br/>
_Sudden illness in the family **deranged** our plans for a trip._
- [ ] **deride**  嘲笑；愚弄 <kbd>sym</kbd> ridicule, mock, lampoon, jeer, scoff at<br/>
_He was **derided** for making such a mistake._
- [ ] **designate**  指定；指派 <kbd>sym</kbd> stipulate, mark out, denote, point out, distinguish, indicate<br/>
_He was designated by the President as the next Secretary of State._
- [ ] **despatch**  殺；速辦 <kbd>sym</kbd> slay, slaughter, expedite, hasten, quicken<br/>
_He **despatched** the bear with one blow on the head._
- [ ] **despoil**  奪取；搶刧 <kbd>sym</kbd> denude, divest, deprive, plunder, ravage<br/>
_The cities of Greece and Asia were **despoiled** of their most valuable ornaments._
- [ ] **destine**  指定；命運注定  <kbd>sym</kbd> ordain, apoint, allot, design, intend, decree, doom, predestine<br/>
_They were **destined** never to meet again._
- [ ] **detach**  分開；分遣 <kbd>sym</kbd> disjoin, separate, unfix, disconnect, divide<br/>
_Men were **detached** to defend the pass._
_ [ ] **detain**  使延遲；拘留 <kbd>sym</kbd> stay, check, retain, keep back, hold, confine<br/>
_He got home two hours late and said he had been **detained** in the office by business._
- [ ] **detest**  憎恨  <kbd>sym</kbd> abominate, hate, abhor<br/>
_I **detest** being interrupted._
- [ ] **dethrone**  癈 (君) <kbd>sym</kbd> depose, drive out of power<br/>
_The **dethroned** king was finally executed._
- [ ] **devastate**  使荒癈；破壞 <kbd>sym</kbd> pillage, plunder, ravage, spoil, destroy<br/>
_A long war **devastated** Europe._
- [ ] **deviate**  逸出正軌；離題 <kbd>sym</kbd> diverge, deflect, veer, digress, slew, err, stray, wander, lose one's way, turn aside<br/>
_His remarks usually **deviate** from truth._
- [ ] **dignify**  使尊貴；使顯貴 <kbd>sym</kbd> promote, elevate, advance, grace, exalt<br/>
_The farmhouse is **dignified** by the great elms around it._
- [ ] **digress**  離開本題 <kbd>sym</kbd> wander, deviate, turn aside<br/>
_His essay **digressed** from the main subject._
- [ ] **dilate**  使膨脹；使擴大 <kbd>sym</kbd> tend, expand, enlarge, distend, widen<br/>
_Gas **dilates** the balloon._
- [ ] **dilute**  稀釋；沖淡 <kbd>sym</kbd> thin, attenuate, weaken, reduce, make weak<br/>
_Strong acids are often **diluted** before they are used._
- [ ] **diminish**  減少；縮小 <kbd>sym</kbd> decrease, abate, lessen, reduce, dwindle<br/>
_The long war greatly **diminished** the country's wealth._
- [ ] **disallow**  不允；拒絕  <kbd>sym</kbd> forbid, prohibit, reject, deny, disavow, disapprove, decline<br/>
_The judge **disallowed** his claim._
- [ ] **disarm**  繳械；解除武裝 <kbd>sym</kbd> lay down arms, disband troops<br/>
_Germany and Japan were **disarmed** after their defeat._
- [ ] **disarrange**  擾亂 <kbd>sym</kbd> disorder, unsettle, derange, disturb<br/>
_The wind **disarranged** her hair._
- [ ] **disband**  解散 (軍隊) <kbd>sym</kbd> separate, disperse, scatter, dismiss<br/>
_The army was **disbanded** when the war came to an end._
- [ ] **disclaim**  拒絕承認；否認 <kbd>sym</kbd> disown, deny, disavow, reject, renounce, cast off<br/>
_He **disclaimed** the ownership of the dog._
- [ ] **discomfit**  挫敗；使混亂 <kbd>sym</kbd> overthrow, subduce, overpower, defeat, beat<br/>
_The teacher was completely **discomfited** by the unexpected question_
- [ ] **discompose**  使不安；弄亂 <kbd>sym</kbd> disorder, disarrange, disturb, jumble, disquiet<br/>
_The grins of his friends **discomposed** Jack when he tried to make his speech._
- [ ] **disconcert**  使驚惶；妨礙 <kbd>sym</kbd> defeat, frustrate, baffle, upset, discompose<br/>
_Henry was **disconcerted** to find that he had come to school without combing his hair._
- [ ] **disown**  否認 <kbd>sym</kbd> disavow, reject, deny, refuse to<br/>
_The boy was so wicked that his father **disowned** him._
- [ ] **disparage**  輕視；毀謗；貶抑 <kbd>sym</kbd> decry, belittle, depreciate, undervalue, traduce<br/>
_The coward **disparaged** the hero's brave rescue._
- [ ] **dissect**  解剖；詳細研究 <kbd>sym</kbd> anatomise, analyze, cut in pieces<br/>
_The lawyer **dissected** the testimony to show where the witnesses had contradicted themselves._
- [ ] **distract**  分心；困惱 <kbd>sym</kbd> divert, perplex, confuse, bewilder<br/>
The music of the radio **distracted** me from my reading._
- [ ] **disunite**  使分離 <kbd>sym</kbd> disjoin, separate, dissociate, divide<br/>
_I am now a lonely being, **disunited** from society._
- [ ] **diverge**  分岐；差異 <kbd>sym</kbd> divaricate, separate, radiate, divide<br/>
_Their paths **diverged** at the fork in the raod._
- [ ] **diversify**  使變化 <kbd>sym</kbd> vary, make different<br/>
_Hills and woods **diversify** the landscape._
- [ ] **divulge**  宣布；洩漏 <kbd>sym</kbd> communicate, disclose, reveal, make public, publish, tell<br/>
_The traitor **divulged** secret plans to the enemy._
- [ ] **dodge** 躲開；逃避責任 <kbd>sym</kbd> start aside, evade, quibble<br/>
_I **dodged** behind a tree so that they should not see me._
- [ ] **doze**  小睡 <kbd>sym</kbd> nap, sleep, slumber, drowse<br/>
_Some students **dozed** off during the lecture._
- [ ] **drizzle**  下細雨 <kbd>sym</kbd> mizzle, rain<br/>
_The day was *drizzling**._
- [ ] **drub**  棒打；強迫灌注 <kbd>sym</kbd> cudgel, beat, thrash, cane, thump<br/>
_You cannot **drub** this idea into him._
- [ ] **edify**  陶冶；教化 <kbd>sym</kbd> upbuild, nurture in religion, improve, teach<br/>
_They tried to **edify** the child with music._
- [ ] **educe**  引出；推斷 <kbd>sym</kbd> draw out, extract, elicit, bring out<br/>
_The teacher's questions **educed** many facts about home gardens._
- [ ] **efface**  塗抺；消除 <kbd>sym</kbd> expunge, blot, rub out, wipe out, rub off, obliterate, erase, cancel<br/>
_It seemed that the very memory of her was **effaced** from his mind._
- [ ] **effectuate**  使實現 <kbd>sym</kbd> secure, accomplish, achieve, carry out<br/>
_The disease did not prevent him from **effectuating** his plan._
- [ ] **effervesce**  沸騰；冒泡 <kbd>sym</kbd> foam, bubble, ferment<br/>
_That hot spring was **efferverscing** with bubbles._
- [ ] **effloresce**  開花 <kbd>sym</kbd> bloom, burst into bloom<br/>
_This plant **effloresces** only once during its life cycle._
- [ ] **effuse**  流出；散布 <kbd>sym</kbd> spill, shed, issue, come forth<br/>
_The town **effuses** warmth and hospitality._
- [ ] **eject**  噴出；逐出 <kbd>sym</kbd> discharge, emit, expel, oust, dismiss, put out<br/>
_The valcano **ejected** lava and ashes._
- [ ] **elapse**  (時間)流逝 <kbd>sym</kbd> pass, lapse, slip away, pass away<br/>
_Five years have **elapsed** since I graduated from the school._
- [ ] **elude**  躲避；困惑 <kbd>sym</kbd> evade, avoid, escape, steal away, baffle, foil, balk, disconcert<br/>
_The fox **eluded** the dogs._
- [ ] **emaciate**  使瘦弱 <kbd>sym</kbd> reduce in flesh, make thin<br/>
_A long illness had **emaciated** the invalid._
- [ ] **embalm**  銘記；使瀰香氣 <kbd>sym</kbd> preserve, cherish, scent, perfume<br/>
_Many fine sentiments are **embalmed** in poetry._
- [ ] **embezzle**  盜用 (公款；公物) <kbd>sym</kbd> misappropriate, steal, defalcate<br/>
_The cashier **embezzled** $50,000 from the bank and ran away._
- [ ] **embitter**  使苦；激怒 <kbd>sym</kbd> aggravate, exasperate, make bitter<br/>
_The loss of all his money **embittered** the old man._
- [ ] **embosom**  包圍；納諸懷中 <kbd>sym</kbd> enfold, nurse, faster, cherish, envelop<br/>
_She is glad to **embosom** his affections._
- [ ] **emend**  修訂 <kbd>sym</kbd> amend, improve, rectify<br/>
_Many men are needed to **emend** this book._
- [ ] **emulate**  與...競爭；效法 <kbd>sym</kbd> vie with, compete with, rival; imitate<br/>
_The proverb tells us to **emulate** the industry of the ant._
- [ ] **enact**   制定為法律 <kbd>sym</kbd> decree, ordain, pass into a law<br/>
_You are required to fulfill these obligations as by law **enacted**._
- [ ] **encompass**  包圍；封入 <kbd>sym</kbd> surround, compass, besiege, include<br/>
_The atmosphere **encompasses** the earth._
- [ ] **encumber**  阻礙行動；煩擾 <kbd>sym</kbd> clog, load, oppress, hinder, obstruct<br/>
_The girl's long skirt **encumbered** her while running._
- [ ] **endorse**  確認；贊同；支持 <kbd>sym</kbd> confirm, approve, support<br/>
_Parents heartly **endorsed** the plan for a school playground._
- [ ] **endue**  賦予 <kbd>sym</kbd> invest, supply, enrich, endow<br/>
_The saint was **endued** with power from God._
- [ ] **energize**  給與精力；用力 <kbd>sym</kbd> give force to, give energy to, animate<br/>
_New forces are **energizing** all about us._
- [ ] **enervate**  使衰弱 <kbd>sym</kbd> weaken, enfeeble, effeminate, render feeble<br/>
_A hot climate **enervates** people who are not used to it._
- [ ] **engender**  產生 <kbd>sym</kbd> generate, beget, produce, cause, bear<br/>
_Crime is often **engendered** by poverty._
- [ ] **enlist**  使入伍 <kbd>sym</kbd> levy, enroll, register, engage<br/>
_We'll **enlist** him in our movement._
- [ ] **enrage**  激怒 <kbd>sym</kbd> irritate, madden, infuriate, provoke, make furious, exasperate<br/>
_He was **enraged** at the insult._
- [ ] **enrapture**  使狂喜；使出神 <kbd>sym</kbd> enchant, enravish, beautify, entrance<br/>
_I was **enraptured** with the stroke of good fortune._
- [ ] **entail**  惹起；使負擔 <kbd>sym</kbd> settle, impose, transfer<br/>
_Liberty **entails** responsibility._
- [ ] **enthrall**  迷惑；使服從 <kbd>sym</kbd> reduce to servitude, enslave, captivate, charm<br/>
_The explorer **enthralled** the audience with the story of his exciting adventures._
- [ ] **entice**  誘惑 <kbd>sym</kbd> lure, allure, decoy, wile, seduce, coax, tempt, cajole<br/>
_The smell of food **enticed** the hungry children into the hut._
- [ ] **entrap**  以網或陷阱捕捉 <kbd>sym</kbd> catch, seduce, inveigle, involve, perplex<br/>
_The lawyer **entrapped** the witness into contradicting himself._
- [ ] **enumerate**  枚舉；計數 <kbd>sym</kbd> compute, reckon, count, number, recount, sum up<br/>
_He **enumerated** to me the advantages of traveling by train._
- [ ] **equate**  使相等；視為同等 <kbd>sym</kbd> set equal to, make equal, equalize<br/>
_We cannot **equate** material possession with goodness._
- [ ] **erode**  蝕；腐蝕 <kbd>sym</kbd> consume, corrode, eat away, destroy<br/>
_Metals are **eroded** by acids._
- [ ] **eschew**  避開；遠離 <kbd>sym</kbd> shun, keep away from, avoid, keep clear from, have nothing to dowith, flee from<br/>
_A wise person **eschews** bad company._
- [ ] **evince**  表明；表示 <kbd>sym</kbd> show, prove, manifest, exhibit<br/>
_The dog **evinced** its dislike of strangers by growling._
- [ ] **evoke**  喚起 <kbd>sym</kbd> call forth, summon, call out<br/>
_A good joke **evokes** a laugh._
- [ ] **exculpate**  辯白；使無罪 <kbd>sym</kbd> exonerate, absolve, acquit<br/>
_He exculpated** himself from stealing the money._
- [ ] **exhume**  從墓中掘出 <kbd>sym</kbd> untomb, unbury, disinter<br/>
_The body was **exhumed** and burned._
- [ ] **extenuate**  使 (罪過等) 顯得輕微 <kbd>sym</kbd> diminish, lessen, reduce in size, mitigate<br/>
_Nothing can **extenuate** his guilt._
- [ ] **extol**  頌揚 <kbd>sym</kbd> praise, laud, celebrate, eulogize, applaud<br/>
_The man **extolled** his girl friend to the skies._
- [ ] **extort**  勒索；強索 <kbd>sym</kbd> exact, wrest, force, extract, wring<br/>
_The police used torture to **extort** a confession from his._
- [ ] **extricate**  救出；使解脫 <kbd>sym</kbd> disentangle, clear, liberate, set free<br/>
_Tome **extricated** his younger brother from the barbed-wire fence._
- [ ] **extrude**  擠出；逐出 <kbd>sym</kbd> eject, force out, thrust out, expel<br/>
_The offender is **extruded** as unworthy of an honorable calling._
- [ ] **exude**  製造；裝配 <kbd>sym</kbd> frame, manufacture, devise, ivent, compose, fashion, construct<br/>
_Automobiles are **fabricated** from parts made in different factories._
- [ ] **fabricate**  製造；裝配 <kbd>sym</kbd> frame, manufacture, devise, invent, compose, fashion, construct<br/>
_Automobiles are **fabricated** from parts made in different factories._
- [ ] **falter**  膽怯 <kbd>sym</kbd> hesitate, halt, waver, fail, stammer<br/>
_The soldiers **faltered** for a moment as their captain fell._
- [ ] **famish**  使挨餓 <kbd>sym</kbd> starve, destroy, die of starvation, suffer extreme hunber<br/>
_They were **famishing** for food._
- [ ] **feign**  假裝 <kbd>sym</kbd> sham, counterfeit, simulate, forge, fabricate<br/>
_Some animals **feign** death when in danger._
- [ ] **ferret**  搜索 <kbd>sym</kbd> search out, seek<br/>
_The detective **ferreted** out the criminal._
- [ ] **fester**  化膿 <kbd>sym</kbd> rankle, corrupt, become malignant<br/>
_If you allow a wound to get dirty, it will probably **fester**._
- [ ] **fete**  慶祝 <kbd>sym</kbd> feast, celebrate<br/>
_They **feted** his recovery with ice cream and cake._
- [ ] **fetter**  束縛 <kbd>sym</kbd> shackle, clog, hamper, bind, chain, restrain<br/>
_We reverence tradition but will not be **fettered** by it._
- [ ] **filch**  偷竊 <kbd>sym</kbd> purloin, stel, pilfer<br/>
_They **filched** pencils from the techer's desk._
- [ ] **flare**  閃耀 <kbd>sym</kbd> flicker, flutter, glare, dazzle, shine<br/>
_A gust of wind made the torches **flare**._
- [ ] **flaunt**  飄蕩 <kbd>sym</kbd> flutter, brandish<br/>
_The banners were **flaunting** in the breeze._
- [ ] **flay**  剝~的皮 <kbd>sym</kbd> excoriate, skin, strip the skin from<br/>
_The hunter **flayed** the fox._
- [ ] **fleek**  飾以斑點 <kbd>sym</kbd> streak, dapple, spot<br/>
_The bird's breast is **flecked** with brown._
- [ ] **flout**  嘲弄 <kbd>sym</kbd> mock, jeer, insult, deride, scoff<br/>
_The foolish boy **flouted** his mother's advice._
- [ ] **fluster**  使慌亂 <kbd>sym</kbd> heat, excite, confuse, glow<br/>
_The honking of horns **flustered** the driver._
- [ ] **foist**  蒙騙 <kbd>sym</kbd> thrust, impose<br/>
_The dishonest shopkeeper **foisted** inferior goods on his customers._
- [ ] **fret**  激怒 <kbd>sym</kbd> chafe, rub, abrade<br/>
_What are you **fretting** about?_
- [ ] **fritter**  切碎 <kbd>sym</kbd> cut into small pieces, slice<br/>
_The cook **frittered** the carrot._
- [ ] **fulminate**  猛烈爆發 <kbd>sym</kbd> detonate, blow out, explode<br/>
_The dynamite suddenly **fulminated**._
- [ ] **gad**  閒逛 <kbd>sym</kbd> ramble about, run loose<br/>
_He was always **gadding** up and down the world._
- [ ] **gag**  將布等塞於口中使不能發言或聲張 <kbd>sym</kbd> stifle, muzzle, choke, muffle, retch<br/>
_The robbers **gagged** the girl to prevent her from crying for help._
- [ ] **gainsay**  否認；反駁 <kbd>sym</kbd> contradict, oppose, deny, controvert, dispute, forbid<br/>
_She is a fine woman-that nobody can **gainsay**._
- [ ] **gambol**  歡跳；雀躍 <kbd>sym</kbd> romp, caper, skip, frisk, leap playfully, frolic, hop<br/>
_Lambs **gamboled** in the meadow._
- [ ] **gape**  張嘴 <kbd>sym</kbd> open, yawn, be opened<br/>
_He **gaped** and yawned._
- [ ] **garner**  收蔵 <kbd>sym</kbd> accumulate, gather, deposit, collect, lay in, set by<br/>
_What is cut and **garnered** at harvest time._
- [ ] **garnish**  加裝飾 <kbd>sym</kbd> decorate, adorn, ornament, bedeck, trim<br/>
_They **garnished** the room with modern paintings._
- [ ] **gash**  深切 <kbd>sym</kbd> score, cut deeply, slash, slit<br/>
_He **gashed** the tree with a saw._
- [ ] **gauge**  精確計劃 <kbd>sym</kbd> measure, estimate, adjust<br/>
_It's difficult to **gauge** one's character._
- [ ] **germinate**  發芽 <kbd>sym</kbd> shoot, vegetate, push, put force, grow, generate<br/>
_Seeds **germinate** in the spring._
- [ ] **gibe**  譏笑 <kbd>sym</kbd> scoff, jeer, fleer, sneer, deride, ridicule, twit<br/>
_They **gibed** at my mistakes._
- [ ] **glaze**  使光滑；變為鈍滯 <kbd>sym</kbd> gloss, polish, furbish, grow dim<br/>
_His eyes **glazed**._
- [ ] **gleam**  發閃光；閃現 <kbd>sym</kbd> shine, glimmer, glitter, sparkle, flash, beam<br/>
_The church **gleamed** through trees._
- [ ] **glean**  收集 <kbd>sym</kbd> collect, gather, harvest, pick up<br/>
_They learned what they wanted to know by **gleaning** through the library._
- [ ] **gloat**  貪婪地看 <kbd>sym</kbd> gaze, stare, look intently, gaze earnestly<br/>
_The miser *gloated** over his gold._
- [ ] **glut**  使吃飽 <kbd>sym</kbd> satiate, cloy, sate, overfeed<br/>
_The boys **glutted** themselves with cake._
- [ ] **gnaw**  噛 <kbd>sym</kbd> bite at, chew, nibble, champ<br/>
_The dog was **gnawing** (at) a bone.<br/>
- [ ] **goad**  剝激；煽動 <kbd>sym</kbd>  harass, worry, annoy, irritate, incite, instigate, urge, stimulate, spur, impel<br/>
_Hunger **goaded** him to steal a loaf of bread._
- [ ] **gorge**   刺激；煽動 <kbd>sym</kbd> devour, swallow, eat, bolt<br/>
_He **gorged** himself._
- [ ] **grip**  抓住 <kbd>sym</kbd> clutch, grasp, seize, lay hold of<br/>
_The frightened child **gripped** its mother's arm.
- [ ] **grope**  摸索 <kbd>sym</kbd> pick one's way, feel one's way<br/>
_He **groped** for the doorhandle in the dark._
- [ ] **grumble**  喃喃訴告 <kbd>sym</kbd> murmur, complain, croak, find fault<br/>
_He is always **grumbling**._
- [ ] **halt**  立定；躊躇 <kbd>sym</kbd> hold, stand, pull up, hobble, walk lamely<br/>
_The troops **halted** for a rest._`
- [ ] **harass**  侵擾 <kbd>sym</kbd> tire, weary, fatigue, vex, worry, trouble, distress, disturb<br/>
_In olden times the coast of England were **harassed** by the Vikings._`
- [ ] **harrow**  使偒心 <kbd>sym</kbd> tear, rend, torment, harass<br/>
_She told us a **harrowing** tale of misfortunes._
- [ ] **harry**  掠奪；困擾 <kbd>sym</kbd> plunder, rob, ravage, raid, annoy, worry, plague, trouble, vex, distrub<br/>
_The pirates **harried** the towns along the coast._
- [ ] **haul**  拖；拉 <kbd>sym</kbd> draw, drag, pull, tug, lug<br/>
_The horses **huled** the logs to the mill._
- [ ] **heave**  用力舉起 <kbd>sym</kbd> hoist, raise up, elevate, lift, throw, toss<br/>
_He **heaved** the heavy box into the wagon._
- [ ] **hinge**  依..而定 <kbd>sym</kbd> depend, hang, be dependent<br/>
_His acceptance will **hinge** upon the terms._
- [ ] **howl**  咆哮 <kbd>sym</kbd> yell, cry, yowl, bawl, wail<br/>
_We heard wolves **howling** in the distance._
- [ ] **ignite**  使燃著 <kbd>sym</kbd> set fire to, kindle, inflame, set on fire<br/>
_You **ignite** a match by scratching it._
- [ ] **imbibe**  飲 <kbd>sym</kbd> drink, absorb, take in, suck up, assimilate<br/>
_He **imbibes** vast quantities of strong coffee._`
- [ ] **imbue**  浸染 <kbd>sym</kbd> dye, stain, tinge, color<br/>
_The detective found a shirt **imbued** with blood._
- [ ] **immolate**  犠牲 <kbd>sym</kbd> offer in sacrifice, sacrifice, kill as a victim<br/>
_The women and children were **immolated** on funeral pyres._
- [ ] **impair**  損害 <kbd>sym</kbd> vitiate, deteriorate, injure, damage, spoil, diminish, lessen, reduce, make less<br/>
_His pleasure was **impaired** by worry about money._
- [ ] **impale**  圍以木椿；刺穿 <kbd>sym</kbd> fence, fierce through<br/>
_She had the butterflies **impaled** on small pins._
- [ ] **impeach**  非難；指責 <kbd>sym</kbd> accuse, arraign, discredit, call in question<br/>
_Do you **impeach** my motives_
- [ ] **implicate**  牽連 <kbd>sym</kbd> infold, involve, entangle, bring into connection with<br/>
_The minister's confession **implicated** numerous officails in the bribery scandal._
- [ ] **importune**  不斷地請求 <kbd>sym</kbd> urge, press, ask urgently<br/>
_She **importuned** her husband for more money._
- [ ] **imprecate**  詛咒 <kbd>sym</kbd> call down, maledict, excrate<br/>
_The prophet **imprecated** ruin on his enemies._
- [ ] **improvise**  即席而作 <kbd>sym</kbd> invent offhand, compose offhand, provide offhand<br/>
_Fred **improvised** a new verse for the school song at the football game._
- [ ] **imcapacitate**  使不能 <kbd>sym</kbd> make incapable, disable<br/>
_His poor health **incapacitated** him for work._
- [ ] **incarcerate**  監禁 <kbd>sym</kbd> put in prison, imprison<br/>
_He is **incarcerated** in his own sensibility._
- [ ] **incriminate**  控告；使負罪 <kbd>sym</kbd> impeach, accuse, blame, inculpate, criminate<br/>
_Don't say anything that may **incriminate** your friends._
- [ ] **inculcate**  諄諄教誨；鼓吹 <kbd>sym</kbd> enforce, instill, infuse, inspire, teach<br/>
_They failed to **inculcate** students with love of knowledge._
- [ ] **indemnify**  使案全；償付 <kbd>sym</kbd> save, secure, compensate, make restitution to<br/>
_He promised to **indemnify** me for my losses._
- [ ] **indent**  以契約束縛 <kbd>sym</kbd> indenture<br/>
_Many persons came to the colonies **indented** for several years._
- [ ] **indite**  著作 <kbd>sym</kbd> write, compose<br/>
_I **indited** a poem._
- [ ] **infringe**  侵犯；違反 <kbd>sym</kbd> violate, break, break through, trespass, intrude<br/>
_Be careful not to **infringe** the rights of other people._
- [ ] **insinuate**  迂迴進入；暗示 <kbd>sym</kbd> instill, infuse, introduce, inculcate, suggest, hint<br/>
_He **insinuated** his doubt of her ability._
- [ ] **instigate** 鼔勵 <kbd>sym</kbd> prompt, incite, urge, stimulate, agitate, stir up<br/>
_Tom **instigated** a quarrel between Jim and Joe._
- [ ] **intensify**  使強 <kbd>sym</kbd> deepen, strengthen, heighten<br/>
_Blowing a fire **intensifies** the heat._
- [ ] **intrude**  侵擾 <kbd>sym</kbd> impose, obtrude, thrust in, force in<br/>
_I hope I am not **intruding**._
- [ ] **inure**  使慣於；鍛鍊 <kbd>sym</kbd> train, habituate, toughen<br/>
_Poverty had **inured** the beggar to hardship._
- [ ] **invert**  倒轉 <kbd>sym</kbd> upset, overturn, turn upside down<br/>
_If you **invert** "I can", you have "Can I?"_
- [ ] **invoke**  懇求 <kbd>sym</kbd> supplicate, pray, implore, beg, entreat, pray to, appeal to<br/>
_The condemned criminal **invoked** the judge's mercy._
- [ ] **isolate**  使隔離 <kbd>sym</kbd> separate, detach, segregate, set apart<br/>
_Several villages have been **ioslated** by the heavy snowfall._
- [ ] **jangle**  爭吵 <kbd>sym<kbd> bicker, wrangle, squabble, chatter, talk idly, gossip<br/>
_The three men **jangled** over trifles._
- [ ] **jar**  震動 <kbd>sym</kbd>shake, vibrate, clash<br/>
_Your heavy footsteps **jar** my table._
- [ ] **jeer**  揶揄 <kbd>scoff, mock, sneer, fleer, deride, gibe<br/>
_Do not **jeer** at the mistakes or misfortunes of others._
- [ ] **jerk**  急拉 <kbd>sym</kbd> twitch, pull suddenly<br/>
_He **jerked** the fish out of the water._
- [ ] **jettison**  投棄 <kbd>cast, throw, scatter<br/>
_We **jettisoned** an unworkable plan._
- [ ] **jog**  輕撞；提醒 <kbd>sym</kbd> jostle, remind, notify<br/>
_In old days torture was used to **jog** the memory of prisoners who would not betray their friends._
- [ ] **jolt**  搖動 <kbd>sym</kbd> jar, bounce, shake, shock<br/>
_The old car **jolted** its passengers badly as it went over the rough road._
- [ ] **jumble** 混雜 <kbd>sym</kbd> throw together in disorder, mix confusedly, confuse, huddle<br/>
_The untidy boy's toys, boots and books were all **jumbled** together in a cupboard._
- [ ] **lampoon** 寫文章 <kbd>sym</kbd> satirize, lash, ridicule<br/>
_The minister was viciously **lampooned** by cartoonists._
- [ ] **languish** 變衰弱 <kbd>sym</kbd> pine, faint, droop, wither, sink, decline, become feeble<br/>
_The flowers **languished** from lack of water._
- [ ] **lash**  打 <kbd>sym</kbd> scourage, flog, whip, castigate, chastise<br/>
_He **lashed** the horse across the back with his whip._
- [ ] **laud**  讚美 <kbd>sym</kbd> praise, celebrate, extol<br/>
_The teacher **lauds** the student to the skies._
- [ ] **lease**  租 <kbd>sym</kbd> let, hire, rent, charter<br/>
_He **leased** his home for the summer._
- [ ] **legalize**  合法化；法律認可 <kbd>sym</kbd> sanction, permit by law, authorize, legitimate<br/>
_The documents should be **legalized** at the nearest consulate._
- [ ] **liberate**  釋放 <kbd>free, emancipate, manumit<br/>
_This will **liberate** him from economic worry._
- [ ] **loathe**  厭惡 <kbd>sym</kbd> feel disgust at, abhor, abominate<br/>
_You **loathe** the smell of greasy food when you are seesick._
- [ ] **loot**  掠奪 <kbd>sym</kbd> ransack, plunder, rifle<br/>
_They **looted** the city after taking it._
- [ ] **lug** 用力拖拉 <kbd>sym</kbd> tug, pull<br/>
_The children **lugged** home a big Christmas tree._
- [ ] **lure**  引誘 <kbd>allure, entice, decoy, tempt<br/>
_Don't let the pleasure of city life **lure** you away from your studies._
- [ ] **lurk** 藏躲 <kbd>sym</kbd> lie concealed, lie hid, keep out of sight<br/>
_There is a suspicious man **lurking** in the shadows._
- [ ] **maculate**  弄污 <kbd>sym</kbd> stain, spot, blur, blotch<br/>
_He **maculated** her clothes._
- [ ] **maim** 使殘廢 <kbd>sym</kbd> mutilate, disable, cripple<br/>
_He was seriously **maimed** in the war._
- [ ] **manipulate**  操作 <kbd>sym</kbd> operate, work, handle<br/>
_The pilot of an airplane must **manipulate** various controls._
- [ ] **masticate**  吃 <kbd>sym</kbd> eat, chew, manducate<br/>
_American now **masticate** 86 million pounds of meat every day._
- [ ] **maze**  使迷惘 <kbd>sym</kbd> amaze, bewilder, confuse, perplex, confound<br/>
_He was so **mazed** that he didn't know what to do._
- [ ] **meander**  蜿蜒而流 <kbd>sym</kbd> wind, turn<br/>
_A brook **meanders** through the meadow._
- [ ] **meddle**  干預 <kbd>sym</kbd> intermeddle, interfere, interpose<br/>
_Who has been **meddling** with my papers?_
- [ ] **mediate**  居中斡旋 <kbd>sym</kbd> intercede, interpose, arbitrate<br/>
_He **mediates** between such a reward._
- [ ] **merit**  應得 <kbd>sym</kbd> deserve, be entitled to<br/>
_He certainly **merits** such a reward._
- [ ] **mete**  分配 <kbd>sym</kbd> measure<br/>
_Justice was **meted** out to them._
- [ ] **mince**  切碎 <kbd>sym</kbd> cut into small pieces, chop, fine<br/>
_She **minced** the ham._
- [ ] **mollify**  緩和 <kbd>sym</kbd> make soft, pacify, appease, assuage, mitigate, moderate<br/>
_He tried to **mollify** his father's anger by apologizing._
- [ ] **muddle**  弄糟 <kbd>sym</kbd> make, turbid, make muddy<br/>
_You have **muddled** the scheme completely._
- [ ] **nauseate**  使噁心 <kbd>sym</kbd> feel nausea, sicken, turn one's stomach<br/>
_Many dishes are commended in one age that are **nauseated** in another._
- [ ] **nettle**  激怒 <kbd>sym</kbd>fret, chafe, ruffle, vex, irritate, harass, incesnse, sting<br/>
_She looked **nettled** by my remarks_
- [ ] **nurture**  養育 <kbd>sym</kbd> feed, nourish<br/>
_She **nurtured** the child as if he had been her own._
- [ ] **obliterate**  消滅 <kbd>sym</kbd> erase, efface, rub out, blot out, wipe out<br/>
_The heavy rain **obliterated** all footprints._
- [ ] **obtrude**  闖入 <kbd>sym</kbd> thrust in, intrude, press in, interfere<br/>
_You had bettwer not **obtrude** your opinions upon others._
- [ ] **obviate**  排除 <kbd>sym</kbd> prevent, remove, turn aside, preclude<br/>
_We tried to **obviate** the necessity of beginning again._
- [ ] **originate**  發起 <kbd>sym</kbd> create, bring into existence, arise, rise, spring, begin, proceed<br/>
_The quarrel **originated** in misunderstanding._
- [ ] **palliate**  減輕 <kbd>sym</kbd> extenuate, excuse, apologize for, ease, mitigate, allay, moderate<br/>
_I shall never attempt to **palliate** my own foibles by exposing the error of another._
- [ ] **palpitate** 跳動；顫抖 <kbd>sym</kbd> throb, pulsate, tremble, quiver, shiver<br/>
_Your heart **palpitates** when you are excited._
- [ ] **parch**  使焦乾 <kbd>sym</kbd> scorch, burn, dry up<br/>
_Thte ground was **parched** with heat._
- [ ] **parry**  躲開 <kbd>sym</kbd> turn aside, prevent, evade, avoid<br/>
_The knife **parried** the blow from the traitor's sword._
- [ ] **perculate**  盜用 <kbd>sym</kbd> appropriate, embezzle, steal, rob, pilfer<br/>
_They accused him of having **perculated** the public money._
- [ ] **perturb**  擾亂 <kbd>sym</kbd> trouble, agitate, distress, excite, worry, vex, perplex, confuse, disorder<br/>
_Mother was much **perturbed** by my illness._
- [ ] **pester**  使困擾 <kbd>sym</kbd> annoy, disturb, harass, provoke, trouble, plague, bother<br/>
_We were **pestered** with flies._
- [ ] **petrify**  變為化石；使發呆 <kbd>sym</kbd> fossilize, rapidify, take away power to think<br/>
_He was **petrified** with terror._
- [ ] **pinion**  束縛 <kbd>sym</kbd> restrain, fasten, shackle, maim<br/>
_The robber **pinioned** his arms._
- [ ] **placate**  安撫；和解 <kbd>sym</kbd> appease, pacify, conciliate<br/>
_He never attempts to **placate** his enemies._
- [ ] **plash**  激發 <kbd>sym</kbd> splash, dash, spatter<br/>
_She **plashed** the water with her legs._
- [ ] **plow**  耕作 <kbd>sym</kbd> till, cultivate<br/>
_The land **plows** hard after the drought._
- [ ] **pluck**  摘；拉 <kbd>sym</kbd> cull, gather, jerk, draw<br/>
_The child **plucked** at his mother's skirt._
- [ ] **plunder**  搶奪 <kbd>sym</kbd> spoil, despoil, rob, harry, pillage, ravage<br/>
_The enemy **plundered** all the goods they found._
- [ ] **plunge**  投入 <kbd>sym</kbd> submerge, dip, sink, put under water<br/>
_Don't **plunge** your hand into hot water._
- [ ] **ply**  使用 <kbd>sym</kbd> practice, exercise<br/>
_The dressmaker **plies**  her needle._
- [ ] **pollute**  污染 <kbd>sym</kbd> soil, taint, defile, make unclean<br/>
_The water at the bathing beach was **polluted** by refuse from the factory._
- [ ] **prate**  喋喋不休 <kbd>sym</kbd> chatter, gabble, tattle, jabber<br/>
_She **prated** of the good old days._
- [ ] **precipitate**  投下；突發 <kbd>sym</kbd> cast down, fling downward, throw headlong, urge forward, hasten, hurry<br/>
_He **precipitated** himself from the housetop._
- [ ] **preclude**  排除；防止 <kbd>sym</kbd> hinder, hamper, prevent, check, restrain<br/>
_A prior engagement will **preclude** me from coming._
- [ ] **premise**  立前提 <kbd>sym</kbd> lay down beforehand, explain previously, set out, preface<br/>
_Let me **premise** my arguemnt with a bit of history._
- [ ] **presage**  預兆 <kbd>sym</kbd> divine, foretell, predict, prophesy, bode, foreknow<br/>
_The incident is believed to presage war._
- [ ] **prevaricate**  支吾其詞 <kbd>sym</kbd> shuffle, quibble, palter, evade the truth, shift<br/>
_Instead of admitting his guilt, he **prevaricated**._
- [ ] **profane**  褻瀆 <kbd>sym</kbd> pollute, violate, abuse, desecrate, debase<br/>
_Soldiers **profaned** the church when they stabled their horses in it._
- [ ] **promulgate**  宣布 <kbd>sym</kbd> announce, publish, proclaim, advertise, make known, give notice of<br/>
_The king **promulgated** a decree._
- [ ] **propitiate**  慰解 <kbd>sym</kbd> atone, mediate, make propitiation<br/>
_They offered a sacrifice to **propitiate** the gods._
- [ ] **propound**  提出 <kbd>sym</kbd> propose, exhibit, set forth<br/>
_He won the prize by **propounding** the theory._
- [ ] **prorogue**  休會 <kbd>sym</kbd> adjourn, postpone<br/>
_Parliament stands **prorogued**._
- [ ] **proscribe**  禁止 <kbd>sym</kbd> exile, ostracize, banish, expel, exclude, outlaw<br/>
_In earlier days, the church proscribed dancing and cardplaying._
- [ ] **protract**  延長 <kbd>sym</kbd> continue, prolong, lengthen<br/>
_They **protracted** their visit for some weeks._
- [ ] **protrude**  伸出 <kbd>sym</kbd> thrust, push forth, shoot out, project<br/>
_The saucy child **protruded** her tongue._
- [ ] **provoke**  激怒 <kbd>sym</kbd> stimulate, excite, arouse, incite, awaken, animate, stir up, induce<br/>
_Don't **provoke** the animals in the cage._
- [ ] **pulsate**  有規律的跳動 <kbd>sym</kbd> throb, palpitate, beat, vibrate<br/>
_His pulses **pulsate** regularly._
- [ ] **purge**  洗清 <kbd>sym</kbd> clear, cleanse, clarify, defecate<br/>
_He **purged** himself of suspicion._
- [ ] **quell**  壓制 <kbd>sym</kbd> supress, subdue, put down, conquer, overpower, restrain, check, repress, extinguish<br/>
_He couldn't **quell** his fears._
- [ ] **quibble**  模稜兩可地說 <kbd>sym</kbd> shuffle, equivocate, prevaricate<br/>
_Don't **quibble** over the thing._
- [ ] **quiver**  振動 <kbd>sym</kbd> shake, tremble, quake, shudder, vibrate, flicker<br/>
_The skylark **quivered** its wing._
- [ ] **rally**  重整 <kbd>sym</kbd> restore, reunite, recover, revive, encourage<br/>
_The commander was able to **rally** the fleeting troops._
- [ ] **rant**  大聲叫喊；咆哮 <kbd>sym</kbd> declaim, spout, rave, vociferate<br/>
_The acotr **ranted** the scene on the stage._
- [ ] **ratify**  批淮 <kb>indorse, confirm, substantiate, corroborate, settle, establish, approve<br/>
_The treaty has been **ratified**._
- [ ] **ravage**  破壞 <kbd>sym</kbd> spoil, ruin, waste, destroy<br/>
_The forest fire **ravaaged** many miles of country._
- [ ] **raze**  破壞 <kbd>sym</kbd> demolish, overthrow, fell, destroy, ruin<br/>
_The old school was **razed** to the ground and a new one was built._
- [ ] **rebuke**  斥責 <kbd>sym</kbd> reprove, censure, reprehend, chide, blame, reprimand, admonish, reproach<br/>
_He **rebuked** him strongly for his negligence._
- [ ] **rebut**  <kbd>sym</kbd> rebuff, repel, oppose, disprove, retrot<br/>
_He **rebutted** the argument of the other team in a debate._
- [ ] **recant**  公然撤回 <kbd>sym</kbd> revoke, recall, adjure, disavow, renounce<br/>
_The tortures could not make the man **recant**._
- [ ] **recaptitulate** 簡述要旨；反復 <kbd>sym</kbd> summarize, repeat, restate<br/>
_So much for the detailed argument, I will now **recapitulate**._
- [ ] **reconcile**  和解 <kbd>sym</kbd> pacify, conciliate, placate, appease<br/>
_The children quarreled but soon **reconciled**._
- [ ] **recruit**  補充 <kbd>sym</kbd> replenish, repair, revive, restore, regain<br/>
_Before sailing, we **recruited** our provisions._
- [ ] **reef**  收 (帆)；收縮 <kbd>sym</kbd> shorten, take in<br/>
_The captain ordered to **reef** the sails for docking._
- [ ] **reek**  發出水汽或煙；發出臭味 <kbd>sym</kbd> steam, emit, exhale, vapor<br/>
_He **reeked** of garlic._
- [ ] **rehabilitate**  恢復 <kbd>sym</kbd> restore, reestablish, renew, reinstate<br/>
_The old house is to be **rehabilitated**._
- [ ] **reimburse**  償還 <kbd>sym</kbd> repay, payback, refund<br/>
_You **reimburse** a person for expenses made for you._
- [ ] **reiterate**  重述 <kbd>sym</kbd> repeat, do again<br/>
_The boy did not move though the teacher **reiterated** her command._
- [ ] **relinquish**  放棄 <kbd>sym</kbd> quit, resign, throw away, leave, cast off<br/>
_They had to **relinquish** their plans for a picnic._
- [ ] **rend**  撕破 <kbd>sym</kbd> sever, break, sunder, rupture, crack, disrupt, tear, dissever<br/>
_They **rent** their clothes._
- [ ] **render**  還給 <kbd>sym</kbd> restore, return, pay back<br/>
_**Render** unto Caesar the things that are Caesar's._
- [ ] **renounce**  否認 <kbd>sym</kbd> reject, disclaim, deny, abjure, decline<br/>
_He **renounced** his wicked son._
- [ ] **renovate**  革新 <kbd>sym</kbd> renew, refresh, reproduce, revivify<br/>
_The fresh aire **renovated** him._
- [ ] **replenish**  補充 <kbd>sym</kbd> stock, fill up, supply, furnish<br/>
_I must **replenish** my wardrobe._
- [ ] **repudiate**  拒絕 <kbd>sym</kbd> reject, discard, disclaim, cast off, nullify, renounce<br/>
_She **repudiated** the gift sent to her._
- [ ] **retrograde**  退後 <kbd>sym</kbd> recede, retire, move backward, retrocede, go backward<br/>
_It was cowardly of him to **retrograde** in face of danger._
- [ ] **reverberate**  回響 <kbd> echo, return, reflect<br/>
_His voice **reverberates** from the high ceiling._
- [ ] **revile**  辱罵 <kbd>sym</kbd> defame, reproach, slander, upbraid, vilify, abuse, malign<br/>
_The tramp **reviled** the man who drove him off._
- [ ] **riddle**  解謎；穿孔 <kbd>sym</kbd> explain, solve, perforate<br/>
_The door of the fort was **riddled** with bullets._
- [ ] **ruffle**  使皺 <kbd>damage, disorder, ripple, disarrange, rumple<br/>
_A breeze **ruffled** the lake._
- [ ] **ruminate**  反芻；沈思 <kbd>sym</kbd> chew, reflect upon, muse on, meditate<br/>
_He was **ruminating** on waht had happened the day before._
- [ ] **saddle**  使負擔 <kbd>sym</kbd> load, burden, clog, charge<br/>
_At his father's death he was **saddled** with heavy debts._
- [ ] **satiate**  使飽；使滿足 <kbd>sym</kbd> sate, satisfy, fill, surfeit, glut, overfill<br/>
_In reviewing a novel, you should try to titillate rather than **satiate** the reader's interest._
- [ ] **saturate**  浸透 <kbd>sym</kbd> fill full, imbue<br/>
_Puring a fog, the air is **saturated** with moisture._
- [ ] **savor**  嚐味 <kbd>sym</kbd> taste, smack, flavor<br/>
_He **savored** the soup with pleasure._
- [ ] **scathe**  損傷 <kbd>sym</kbd> harm, injure, damage, destroy<br/>
_He **scathed** his opponent's honor with rumors._
- [ ] **scoff**  嘲笑 <kbd>sym</kbd> mock, jeer, ridicule, deride, sneer<br/>
_He **scoffed** at the difficulties._
- [ ] **scoop**  掘 <kbd>sym</kbd> hollow out, dig out, remove<br/>
_The children **scooped** up the snow with their hands to build a snow man._
- [ ] **scrape**  刮；積攢 <kbd>sym</kbd> rub, grind, rasp, collect, gather<br/>
_**Scrape** all the rust off._
- [ ] **scratch**  抓傷 <kbd>sym</kbd> scarify, wound slightly<br/>
_He **scratched** his hands badly while pruning the rose bushes._
- [ ] **scribble**  潦草書寫 <kbd>sym</kbd> scrabble, scratch, scrawl<br/>
_No **scribbling** on the walls._
- [ ] **scrub**  (用力)擦洗 <kbd>sym</kbd> cleanse, clean, rub hard<br/>
_He was ordered to **scrub** the floor._
- [ ] **scrutinize**  細察 <kbd>sym</kbd> examine, search, investigate<br/>
_The jeweler **scrutinized** the diamond for flaws._
- [ ] **scud**  疾行 <kbd>sym</kbd> run, haste, flee, trip, speed, hasten<br/>
_Rabbits **scud** across the turf._
- [ ] **scuffle**  混戰 <kbd>sym</kbd> struggle, fight, strive<br/>
_He **scuffled** with the man who abused him._
- [ ] **scurry**  疾走 <kbd>sym</kbd> hurry, hasten, scutter<br/>
_We could hear the mice **scurrying** about in the walls._
- [ ] **scuttle**  疾走 <kbd>sym</kbd> bustle, hurry, scuffle<br/>
_The dogs **scuttled** off into the woods._
- [ ] **seethe**  沸騰；起泡沬 <kbd>sym</kbd> boil, sruge, bubble<br/>
_Water **seethed** under the falls._
- [ ] **shackle**  加桎梏；束縛 <kbd>sym</kbd> fetter, chain, hamper, manacle<br/>
_They are **shackled** by inherited conventions._
- [ ] **sheathe**  (將力劍) 插入鞘 <kbd>sym</kbd> cover, case, incase<br/>
_Realizing the sound was made by a cat, the guard **sheathed** his sword._
- [ ] **shove**  推擠 <kbd>sym</kbd> propel, push<br/>
_Helen **shoved** the book across the desk to him._
- [ ] **shred**  切成碎片 <kbd>sym</kbd> cut<br/>
_Her reputation was **shredded** by the scandal._
- [ ] **shriek**  發出尖銳的叫聲 <kbd>sym</kbd> screech, squeal, scream, yell<br/>
_She **shrieked** curses at me._
- [ ] **shun**  避開 <kbd>sym</kbd> evade, avoid, escape, get clear of<br/>
_She was lazy and **shunned** work._
- [ ] **skim**  掠過 <kbd>sym</kbd> pass lightly<br/>
_The swallows were **skimming** by._
- [ ] **skulk**  藏匿；潛行 <kbd>sym</kbd> hide, lurk, slink, lie hid, sneak<br/>
_The burglar **skulked** behind the door when he heard someone talking._
- [ ] **slake**  息 (怒) <kbd>sym</kbd> extinguish, allay, abate, quench<br/>
_Nothing can **slake** his anger._
- [ ] **slant**  傾斜 <kbd>sym</kbd> lean, incline, lie obliquely<br/>
_Most handwriting **slants** to the right._
- [ ] **slit**  割裂 <kbd>sym</kbd> cut, divide, rend, split, cleave<br/>
_He **slit** the bag open._
- [ ] **smack**  微有 (某) 味 <kbd>sym</kbd> have a taste, have a flavor<br/>
_The medicine **smacks** of sulphur._
- [ ] **smash**  破碎 <kbd>sym</kbd> mash, shatter, break into pieces<br/>
_The dish **smashed** on the floor._
- [ ] **smear**  塗；弄髒 <kbd>sym</kbd> bedaub, besmear, plaster, smudge<br/>
_She **smeared** her fingers with jam._
- [ ] **sneak**  潛行 <kbd>sym</kbd> move quickly, lurk, slink<br/>
_The man **sneak** about the place watching for a chance to steal something._
- [ ] **sneer**  嘲笑 <kbd>sym</kbd> ridicule, scorn, deride, disdain, despise, look down on, turn up the nose at<br/>
_The proposal was **sneered** down._
- [ ] **sniff**  以鼻吸氣 <kbd>sym</kbd> snuff, inhale, breathe<br/>
_The man who had a cold was **sniffing**._
- [ ] **sojourn**  逗留；寄居 <kbd>sym</kbd> abide, lodge, dwell, reside, rest<br/>
_He **sojourned** with his uncle._
- [ ] **spawn**  產卵 <kbd>sym</kbd> generate, bring forth, produce<br/>
_The fish were madly pushing their way upstream to **spawn**._
- [ ] **squall**  悲鳴；尖叫 <kbd>sym</kbd> cry, scream, cry out, yell, shriek<br/>
_The baby **squalled**._
- [ ] **squander**  浪費 <kbd>sym</kbd> expend, lavish, waste, dissipate<br/>
_He **squandered** his time and money in gambling._
- [ ] **stagger**  蹣跚 <kbd>sym</kbd> sway, totter, reel, waver<br/>
_The drunkard **staggered** across the road._
- [ ] **stalk**  高視闊步 <kbd>sym</kbd> stride, walk, pace, march<br/>
_He **stalked** out of the room._
- [ ] **stare**  凝視 <kbd>sym</kbd> gape, gaze, look intently<br/>
_The sight made me **stare**._
- [ ] **sublimate**  使昇華 <kbd>sym</kbd> refine, elevate, idealize<br/>
_Desires can be **sublimated** into fine poetry._
- [ ] **suffuse**  布滿 <kbd>sym</kbd> spread over, fill<br/>
_At twilight, the sky was **suffused** with color._
- [ ] **sully**  玷污 <kbd>sym</kbd> soil, stain, spot, blemish, blot, contaminate<br/>
_His reputation is **sullied** by many crimes._
- [ ] **supersede**  替代 <kbd>sym</kbd> suspend, overrule, make void, displace<br/>
_Will airplanes **supersede** trains?_
- [ ] **surmise**  臆測 <kbd>sym</kbd> suspect, suppose, imagine, think, guess<br/>
_We **surmised** that the delay was caused by some accident._
- [ ] **swell**  膨脹 <kbd>dilate, expand, distend, fill out<br/>
_If you put it into the water it will **swell**._
- [ ] **swelter**  熱昏 <kbd>sym</kbd> be overcome by heat<br/>
_He is **sweltering** in Florica while we are freezing up here._
- [ ] **synchronize**  同時發生 <kbd>sym</kbd> be simultaneous, concur in time<br/>
_The voyages of discovery **synchronized** with the emergence of capitalist economy._
- [ ] **tally**  符合 <kbd>sym</kbd> match, agree, conform, correspond, harmonize, accord, coincide<br/>
_Your account **tallies** with mine._
- [ ] **tamper**  干預 <kbd>sym</kbd> intermeddle, meddle, interfere<br/>
_Don't **tamper** with other's business._
- [ ] **tantalize**  折磨；使著急 <kbd>sym</kbd> torment, provoke, vex, balk, irritate, tease, frustrate<br/>
_The sight is most **tantalizing**._
- [ ] **teem**  充滿 <kbd>sym</kbd> abound, be full<br/>
_Fish **teem** in this river._
- [ ] **temporize**  見風使舵 <kbd>sym</kbd> hedge, comply with occasions<br/>
_He is always **temporizing** and is disliked by his classmates._
- [ ] **terminate**  終結 <kbd>sym</kbd> limit, end, close, finish, put an end to, set bound to<br/>
_The meeting **terminated** at 10 o'clock._
- [ ] **throttle**  使窒息 <kbd>sym</kbd> strangle, suffocate, choke<br/>
_The criminal **throttled** the watchman and robbed the bank._
- [ ] **thrust**  力推 <kbd>sym</kbd> shove, push, propel, impel<br/>
_They **thrust** themselves forward._
- [ ] **thump**  重擊 <kbd>sym</kbd> knock, strike, punch, batter<br/>
_He **thumped** the table with his fist._
- [ ] **tipple**  常飲；酗酒 <kbd>sym</kbd> guzzle, tope, drink hard<br/>
_He **tipples** brandy._
- [ ] **tolerate** 容忍 <kbd>sym</kbd> permit, admit, allow, endure, bear, put up with<br/>
_He had to **tolerate** his wife's mother._
- [ ] **toll**  鳴 (鐘) <kbd>sym</kbd> draw, allure, draw on<br/>
_Bells were **tolled** all over the country at the King's death._
- [ ] **tow**  拖 <kbd>sym</kbd> draw, drag, pull, tug<br/>
_The broken truck was **towed** to the garage._
- [ ] **traduce**  中偒 <kbd>sym</kbd> defame, vivify, slander, culminate<br/>
_It is not easy to **traduce** his character._
- [ ] **transact**  處理 <kbd>sym</kbd> perform, manage, conduct, treat, enact<br/>
_He **transacts** business with stores all over the country._
- [ ] **transcend**  超越 <kbd>sym</kbd> transgress, pass, overstep, overpass<br/>
_The scene is so beautiful that it **transcends** my powers of description._
- [ ] **transform**  使變形 <kbd>sym</kbd> transfigure, transmute, change<br/>
_The witch **transformed** men into pigs._
- [ ] **transmute**  使變形 <kbd>sym</kbd> change, transform<br/>
_We can **transmute** water power into electrical power._
- [ ] **transpire**  排出；蒸發 <kbd>sym</kbd> exhale, evaporate, pass off in vapor<br/>
_The plants **transpire**  water through the pores on the surface of the leaves._
- [ ] **traverse**  反對；走過 <kbd>sym</kbd> thwart, counteract, frustrate, pass, travel<br/>
_We **traversed** the desert._
- [ ] **tug**  拖曳 <kbd>sym</kbd> draw, haul, pull, drag<br/>
_We **tugged** the boat in to shore._
- [ ] **unearth**  發掘；發現 <kbd>sym</kbd> exhume, uncover, bring to light, disclose<br/>
_The lawyer **unearthed** some new evidence about the case._
- [ ] **upbraid**  譴責 <kbd>reproach, reprove, blame, chide, condemn<br/>
_The captain **upbraided** his men for falling asleep._
- [ ] **uphold**  舉起；撐 <kbd>sym</kbd> elevate, raise, support, sustain<br/>
_Walls **uphold** the roof._
- [ ] **vanish**  消失 <kbd>sym</kbd> go out of sight, disappear, become invisible<br/>
_Your prospects of success have **vanished**._
- [ ] **veer**  (風) 改變方向 <kbd>sym</kbd> turn, shift, change, deviate<br/>
_The wind **veered** round to the west._
- [ ] **vex**  使煩惱 <kbd>sym</kbd> tease, torment, perplex, bewilder, harass, worry, annoy, distress, plague<br/>
_Her continuous chatter **vexes** me._
- [ ] **vindicate**  辯明；主張 <kbd>sym</kbd> justify, uphold, assert, maintain<br/>
_The verdict "Not fuilty" **vindicated** him._
- [ ] **waddle**  蹣跚而行 <kbd>sym</kbd> waggle, tottle, toddle<br/>
_The ducks **waddled** into the pond._
- [ ] **wag**  搖擺 <kbd>sym</kbd> waggle, shake, sway<br/>
_The dog **wags** its tail to welcome you._
- [ ] **wan**  (使) 變蒼白 <kbd>sym</kbd> shrivel, fade, wilt<br/>
_Her face **wanned** after her long illness._
- [ ] **wither**  凋謝 <kbd>sym</kbd> shrivel, fade, wilt<br/>
_The flowers soon **withered**._
- [ ] **wring**  扭 <kbd>sym</kbd> twist, contort<br/>
_**Wring** out your wet clothes._
- [ ] **yell**  號叫 <kbd>sym</kbd> shriek, scream, bawl, howl, cry, shout<br/>
_He **yelled** with pain._
- [ ] **yelp**  喊叫 <kbd>sym</kbd> yap, bark, cry<br/>
_The boy **yelped** in pain when the horse stepped on his foot._