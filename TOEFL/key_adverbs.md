# TOEFL 文法關鍵副詞及容易誤用的字
## 與形容詞同形的副詞
- [ ] **hard**  努力地<br/>
_Barry always works **hard** since he intends to enter the graduate school._<br/>
<kbd>compare</kbd> _Barry is a **hard** workder._
- [ ] **fast**  快速地<br/>
_Frank runs very **fast**._<br/>
<kbd>compare</kbd> _Frank is a **fast** runner._
- [ ] **short**  短暫地；突然地<br/>
_Jenny stopped speaking **short**._<br/>
<kbd>compare</kbd> Jenny made a **short** speech._
- [ ] **early**  早<br/>
_Mike rises **early** in the morning._<br/>
<kbd>compare</kbd> Mike is an **early** riser._

### 類似的其他單字
- [ ] **late** 遲；遲的
- [ ] **high** 高；高的
- [ ] **long** 長；長的
- [ ] **near** 近；近的
- [ ] **far** 遠；遠的
- [ ] **low** 低下地；低的
- [ ] **still**  依然；靜止的
- [ ] **slow** 慢；慢的
- [ ] **well** 很好的；健康的

## 加上 -ly 之後意思完全不同的副詞
- [ ] **hardly** 幾乎不 <kbd>sym</kbd> scarcely<br/>
_Barry **hardly** worked even when he was scolded by the teacher._<br/>
<kbd>compare</kbd> Barry worked **hard** every day.
- [ ] **lately** 最近<br/>
_Mr. Shelby has come here **lately**._<br/>
<kbd>compare</kbd> _Mr. Shelby came **late** in the morning._
- [ ] **nearly**  幾乎<br/>
_Mary was **nearly** attacked by the burglar._<br/>
<kbd>compare</kbd> Mary was coming **near**._

### 類似的其他副詞

- [ ] **close** 接近的
- [ ] **closely** 緊密地

- [ ] **clear** 完全地
- [ ] **clearly** 明亮地

- [ ] **dear** 昂貴地
- [ ] **dearly** 親愛地

- [ ] **pretty** 十分；頗
- [ ] **prettily** 悅人地

- [ ] **short** 急地
- [ ] **shortly** 不久

- [ ] **just** 正巧；剛好
- [ ] **justly** 公正地

- [ ] **most** 最
- [ ] **mostly** 主要地

- [ ] **sharp** 準；正
- [ ] **sharply** 銳利地

### 容易誤用的相似單字

- [ ] **as**
- [ ] **like** <br/>
as 是連接詞，須接子句；like 是介系詞，後面要接名詞為受詞。<br/>
_Mr. Rubin behaved **as** they ordered him to._<br/>
_Mr. Rubin behaved **like** his father.
- [ ] **imply** 暗示
- [ ] **infer** 推知<br/>
_The speaker **implied** that Dr. Battle's idea was wrong._<br/>
_Her audience **inferred** that she would support Dr. Pollard's idea._
- [ ] **beside** 在 ~ 之旁
- [ ] **besides** 並且<br/>
_Mrs. Scherer was sitting **beside** her husband._<br/>
_Mrs. Scherer can speak French **besides** English._
- [ ] **agree to + st** 同意
- [ ] **agree with + sb** 同意<br/>
_James **agreed to** Sarah's plan._<br/>
_James **agreed with** Sarah on this point._
- [ ] **disinterested** 公正的 <kbd>sym</kbd> impartial
- [ ] **uninterested** 冷淡的 <kbd>sym</kbd> not concerned<br/>
_Ronald and Robert are **disinterested** persons._<br/>
_They are **uninterested** in the results._
- [ ] **farther** 更遠的
- [ ] **further** 更進一步的<br/>
_Mr. Brown's home town is **farther** from here than mine._<br/>
_Mr. Brown wants to get **further** information._
- [ ] **emigrate** 移居出境 <kbd>n.</kbd> emigrant 移民<br/>
- [ ] **immigrate** 移居入境 <kbd>n.</kbd> 移民<br/>
_Ting **emigrated** from Africa to the United States._<br/>
_Ting **immigrated** into the United States._
- [ ] **formally** 正式地 <kbd>sym</kbd> officially
- [ ] **formerly** 先前的 <kbd>sym</kbd> previously<br/>
_Mr. Conrad **formally** informed the students of the date._<br/>
_Mr. Conrad was **formerly** a salesman._
- [ ] **affect** 影響
- [ ] **effect** 效果<br/>
_They went to Ethiopia to see how the drought **affected** life there._<br/>
_Their warning had no **effect** on me._
- [ ] **healthy** 健康的
- [ ] **healthful** 於健康有益的<br/>
_Jim became **healthy** by jogging every morning._<br/>
_Bean curd is now regarded as a **healthful** diet._
- [ ] **access**  許可；接近 <kbd>sym</kbd> admission
- [ ] **excess** 過度 <kbd>sym</kbd> abundance<br/>
_The students had no **access** to the teacher's files._<br/>
_He has an **excess** of enthusiasm._
- [ ] **already** 已經 <kbd>sym</kbd> so soon
- [ ] **all ready** 一切就緒 <kbd>sym</kbd> entirely ready<br/>
_The train had **already** left before we got to the station._<br/>
_Alex and Bob are **all ready** to leave for New York._
- [ ] **among** 在 ~ 之中(三者以上)
- [ ] **between** 在兩者之間<br/>
_Prof. Bough is eminent **among** scientists._<br/>
_This is a secret **between** you and me._
- [ ] **remember** 記得
- [ ] **remind** 提醒<br/>
_Mrs. Lehman couldn't **remember** his name._<br/>
_The picture **reminded** Mrs. Lehman of her school-days._
- [ ] **sensible** 可感覺的
- [ ] **sensitive** 敏感的
- [ ] **sensuous** 感覺的<br/>
_Donna is **sensible** of her shortcomings._<br/>
_Donna is **sensitive** to strong smells._<br/>
_Donna likes **sensuous** paintings._<br/>
<kbd>compare</kbd> sensual 感官的
- [ ] **proceed** 繼續
- [ ] **precede** 在先<br/>
_Mr. Campbell **proceeded** to teach the history of England._<br/>
_These kinds of orders **precede** others._
- [ ] **observation** 觀察
- [ ] **observance** 遵守<br/>
_The **observation** of the moon is my homework._<br/>
_We have to practice early rising in **observance** of the rule._
- [ ] **helpless**  無助的
- [ ] **useless** 無效的<br/>
_Joseph was a **helpless** invalid._<br/>
_It is **useless** to help him._
- [ ] **practical** 實用的
- [ ] **practicable** 可實行的<br/>
_The plan that Tony presented was **practical**._<br/>
_The plan that Tony presented was **practicable**._
- [ ] **afflict** 使痛苦 <kbd>sym</kbd> distress, torment
- [ ] **inflict** 予以 <kbd>sym</kbd> impose<br/>
_Mrs. Johnson was **afflicted** with her husband's failure._<br/>
_Mrs. Johnson **inflicted** punishment on her son.
- [ ] **almost** 幾乎 <kbd>sym</kbd> not quite
- [ ] **most** 大多數的<br/>
_**Almost** all the students agreed to Jim's proposal._<br/>
_**Most** of the students agreed to Jim's proposal.
- [ ] **adapt** (使)適應
- [ ] **adopt** 採用
- [ ] **adept** 擅長的<br/>
_Arnold **adapted** to the new environment._<br/>
_Arnold resolved to **adopt** their regulations._<br/>
_Arnold is **adept** at fixing radio sets.
- [ ] **by** 在 ~ 以前
- [ ] **till** 直到
_I'll have finished my homework **by** tomorrow morning._<br/>
_I'll be studying here **till** tomorrow._
- [ ] **beat** 打敗
- [ ] **win** 贏得<br/>
_Our team **beat** Saunders' by a huge score._<br/>
_We **won** the game._
- [ ] **device** 發明或創造的東西
- [ ] **devise** 計畫；發明<br/>
_That is a nice **device** for sharpening logs._<br/>
_Don **devised** a plan for reaching the seashore._
- [ ] **human** 人類的
- [ ] **humane** 仁愛的<br/>
_At least some novelists are interested in **huan** society._<br/>
_Gerald assumed a **humane** attitude to his friends._
- [ ] **intense** 非常的
- [ ] **intensive** 集中的<br/>
_I can't stand this summer's **intense** heat._<br/>
_They took the **intensive** training course._
- [ ] **intelligent** 有智力的 <kbd>sym</kbd> smart
- [ ] **intelligible** 可理解的 <kbd>sym</kbd> understandable
- [ ] **intellectual** 聰明的 <kbd>sym</kbd> wise<br/>
_Mr. Smith is an **intelligent** person._<br/>
_Mr. Smith's theory is not **intelligible** to us._<br/>
_Mr. Smith committed an **intellectual** crime._
- [ ] **compare** 比較。著重於相似處 (show similarities)
- [ ] **contrast** 對比。著重於相異處 (show differences)<br/>
_Linda **compared** the human structure with the bird's._<br/>
_Please **contrast** your pronunciation with your teacher's._
- [ ] **successive** 連續的。指在次序上無間斷
- [ ] **consecutive** 連續的。指綿密或緊接的連續<br/>
_He has worked on three **successive** Saturdays._<br/>
_He worked three **consecutive** days last week._
- [ ] **prophecy** 預言 (n.) <kbd>sym</kbd> prediction
- [ ] **prophesy** 預言 (v.) <kbd>sym</kbd> foretell, predict<br/>
_Mr. Calloway's metapsychological **prophecy** is somewhat accurate._<br/>
_Mr. Calloway **prophesies** the human future metapsychologically._
- [ ] **hardly** 幾乎不 <kbd>sym</kbd> scarcely
- [ ] **rarely** 罕有的 <kbd>sym</kbd> seldom<br/>
_I can **hardly** believe him._<br/>
_I **rarely** trust men._

### 在字型上容易混淆的單字
- [ ] **your** 你的 (你們的)
- [ ] **you're** = you are

- [ ] **their** 他們的
- [ ] **there** 在那裡
- [ ] **they're** = they are

- [ ] **envelop** 包
- [ ] **envelope** 信封

- [ ] **advice** 忠告 (n.)
- [ ] **advise** 忠告 (v.)

- [ ] **lose** 失去
- [ ] **loose** 未予束縛的

- [ ] **quit** 停止
- [ ] **quite** 完全地
- [ ] **quiet** 靜止的

- [ ] **brake** 煞車
- [ ] **break** 打破

- [ ] **bear** 忍受
- [ ] **bare** 赤裸的

- [ ] **coarse** 粗的
- [ ] **course** 課程

- [ ] **weather** 天氣
- [ ] **whether** 是否

- [ ] **stationery** 文具
- [ ] **stationary** 固定的

- [ ] **it's** = it is
- [ ] **its** 它的

- [ ] **costume** 服裝
- [ ] **custom** 習慣
- [ ] **cusume** 消費

- [ ] **forth** 向前
- [ ] **fourth** 第四的

- [ ] **peace** 和平
- [ ] **piece** 一片

- [ ] **house** 房屋
- [ ] **home** 家庭

- [ ] **desert** 沙漠
- [ ] **desert** 放棄
- [ ] **dessert** 甜點

- [ ] **clothes** 衣服
- [ ] **cloths** 布

- [ ] **dye** 顏料
- [ ] **die** 死亡

- [ ] **lesson** 功課
- [ ] **lessen** 減少

### 應避免發生重複 (redundancy) 的單字
下列單字本身的意思即很完全，不必再加相似意義的副詞或形容詞予以修飾。加了~~刪除號~~的字不需要，卻常見到句子中出現而形成冗字。

- [ ] **return** 回來<br/>
_Hassel **returned** ~~back~~ to Germany from Africa after crossing the Sahara._
- [ ] **advance** 前進<br/>
_The leader ordered the man to **advance** ~~forward~~ on their enemy._
- [ ] **reason** 理由<br/>
_The **reason** ~~because~~ I quit smoking is that the doctor advised me to._
- [ ] **enough** 足夠的<br/>
_Mr. Carlson has ~~sufficient~~ **enough** money to buy a new car._
- [ ] **innovation** 新制度；革新<br/>
_They decided to adopt the ~~new~~ **innovation**._
- [ ] **compete** 競爭<br/>
_Mark was **competing** ~~together~~ with the orders for a prize._

#### 類似其它的單字
- [ ] **repeat** 重做  不可用 **repeat** ~~again~~
- [ ] **identical** 同一的  不可用 ~~same~~ **identical**