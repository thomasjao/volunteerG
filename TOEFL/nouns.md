- [ ] **ablution**  淋浴 <kbd>sym</kbd> lavation, washing, cleasing<br/>
_He performed his **ablution** before the ritual._
- [ ] **abstinence**  禁戒；節制 <kbd>sym</kbd> abstaining, temperance<br/>
_His **abstinence** from food made him weak._
- [ ] **abyss**  深淵 <kbd>sym</kbd> gulf, gorge<br/>
_In the **abyss** of his mind he apprehends the world's minuteness._
- [ ] **accomplice**  從犯者 <kbd>sym</kbd> accessory, confederate, abettor<br/>
_Without an **accomplice** the theif could not have got into the house and stolen the jewels._
- [ ] **acme**  頂點 <kbd>sym</kbd> apex, top, summit, zenith, vertex, peak, highest point, pinnacle<br/>
_Schubert reached the **acme** of his skill while quite young._
- [ ] **acumen**  銳敏 <kbd>sym</kbd> shrewdness, sagacity, acuteness, sharpness, discernment, penetration<br/>
_His business **acumen** brought him riches in a short time._
- [ ] **adage**  格言；諺語 <kbd>sym</kbd> saying, proverb, apothegm, maxim<br/>
_I preferred him to the **adage** "A rolling stone gathers no moss."_
- [ ] **adulteration**  攙雜 <kbd>sym</kbd> corruption, debasement, deterioration, sophistication<br/>
_The **adulteration** of wood alcohol in the wine roused public attention._
- [ ] **adversity**  不幸 <kbd>sym</kbd> calamity, misfortune, trouble, disaster, distress, misery, woe<br/>
_His struggles with **adversity** are fruitless._
- [ ] **affinity**  密切關係 <kbd>sym</kbd> connection, relationship<br/>
_English has a close **affinity** to French._
- [ ] **agility**  敏捷；機敏 <kbd>sym</kbd> briskness, liveliness, quickness, promptness, smartness, alertness<br/>
_He has the **agility** of a deer._
- [ ] **alimentation**  營養 <kbd>sym</kbd> nourishment, nutrition, sustentation<br/>
_The boy looked skinny as a result of poor **alimentation**._
- [ ] **allusion**  引述 <kbd>sym</kbd> hint, reference, intimation, suggestion <kbd>v.</kbd> allude<br/>
_His writings are filled with classical **allusions**._
- [ ] **altruism**  利他主義 <kbd>sym</kbd> love of others, devotion to others, philanthropy, unselfishness<br/>
_He practiced **altruism** throughout his life._
- [ ] **amenity**  適意 <kbd>sym</kbd> agreeableness, pleasantness, softness<br/>
_The **amenity** of his manners won him friends._
- [ ] **amnesia**  健忘症 <kbd>sym</kbd> loss of memory<br/>
_He suffered **amnesia** from the accident._
- [ ] **amnesty**  木赦 <kbd>sym</kbd> general pardon, forgiveness, freedom from penalty<br/>
_Order was restored and the king granted **amnesty** to those who had plotted against him._
- [ ] **analogy**  相似 <kbd>sym</kbd> resemblance, similarity, likeness, similitude, parallelism<br/>
_No **analogy** exists between them._
- [ ] **animosity**  憎惡 <kbd>sym</kbd> virulence, malignity, hostility, hatred<br/>
_There exist **animosities** between classes._
- [ ] **anomaly**  異例 <kbd>sym</kbd> abnormity, irregularity, peculiarity<br/>
_A bird that cannot fly is an **anomaly**._
- [ ] **antipathy**  反感 <kbd>sym</kbd> disgust, repugnance, abhorrence, aversion<br/>
_I ave an **antipathy** to snakes._
- [ ] **antiquity**  古代 <kbd>sym</kbd> ancient times, early times<br/>
_Athens is a city of great **antiquity**._
- [ ] **apostasy**  背教 <kbd>sym</kbd> defection, desertion<br/>
_He was looked down upon for **apostasy**._
- [ ] **ardor**  熱心 <kbd>sym</kbd> warmth, ardency, zeal, fervor, excitement<br/>
_The students were much impressed by their teacher's **ardor** in teaching._
- [ ] **arrogance**  傲慢 <kbd>sym</kbd> haughtiness, superciliousness, disdain<br/>
_In that country there is no **arrogance** of the robility toward the common people._
- [ ] **assumption**  擔任；假設 <kbd>sym</kbd> taking on, acceptance, presumption, supposition, hypothesis<br/>
_Your **assumption** is wrong._
- [ ] **asteroid**  小行星 <kbd>sym</kbd> severity, rigor, harshness, rigidity<br/>
_A peculiar **austerity** marked almost all his judgements of men and actions._
- [ ] **autopsy**  驗屍 <kbd>sym</kbd> post-mortem examination<br/>
_An **autopsy** was needed to accertain the cause of his death._
- [ ] **avarice**  貪婪 <kbd>sym</kbd> covetousness, cupidity, graspingness<br/>
_His **avarice** alienated his friends._
- [ ] **awe**  敬畏 <kbd>sym</kbd> veneration, reverence, solemn, exclamation<br/>
_He had a feeling of **awe** as he was taken into the presence of the Emperor._
- [ ] **barricade**  障礙物 <kbd>sym</kbd> obstruction, barrier, bar<br/>
_The soldiers cut trees down to make a **barricade** across the road._
- [ ] **basin**  盆 <kbd>sym</kbd> disk-shaped depression<br/>
_Wash your hands in the **basin**._
- [ ] **beverage** 飲料 <kbd>sym</kbd> potion, drink<br/>
_Tea, coffee, wine, and milk are **beverages**._
- [ ] **bigotry**  固執 <kbd>sym</kbd> intolerance, prejudice, obstinacy<br/>
_He found his teacher's **bigotry** intolerable._
- [ ] **bravado**  虛張的勇氣 <kbd>sym</kbd> boasting, boast, brag, bombast<br/>
_Morale is not based on **bravado** but on real competence._
- [ ] **brim**  (杯、碗)的邊 <kbd>sym</kbd> edge, margin, brink<br/>
_The glass is full to the **brim**._
- [ ] **bully**  土霸 <kbd>sym</kbd> blusterer, roisterer, typrant<br/>
_The street **bullies** ordered the boy to give them all his money._
- [ ] **cabal**  徒黨；陰謀 <kbd>sym</kbd> clique, junto, party, intrigue, plot<br/>
_He was a young poet with his own little **cabal** trailing around after him._
- [ ] **caliber**  直徑；才能 <kbd>sym</kbd> diameter, capacity, scope, ability<br/>
_He is a statesman of the highest **caliber**._
- [ ] **canter**  慢步小跑 <kbd>sym</kbd> trot, amble, gentle, gallop<br/>
_The horse won at a **canter** in the race._
- [ ] **carcass**  屍體；殘骸 <kbd>sym</kbd> dead body, corpse<br/>
_**Carcasses** of old cars lay rusting among the trees._
- [ ] **carrion**  腐臭的肉 <kbd>sym</kbd> putrid meat or flesh<br/>
_We were not so reduced as to eat **carrion**._
- [ ] **casualty**  意外 <kbd>sym</kbd> chance, contingency, hap, fortuity, accident, disaster, misfortune, calamity, catastrophe<br/>
_The driver was responsible for the **casualty**._
- [ ] **cataclysm**  洪水 <kbd>sym</kbd> deluge, inundation, flood<br/>
_The **cataclysm** flooded the entire valley._
- [ ] **catastrophe**  異常的災禍 <kbd>sym</kbd> disaster, cataclysm, mishap, mischance, misfortune<br/>
_A big earthquake is a **catastrophe**._
- [ ] **censor**  檢查員 <kbd>sym</kbd> inspector, faultfinder<br/>
_The **censor** cut out the immoral parts of the film._
- [ ] **cession**  割讓 <kbd>sym</kbd> surrender, yielding, renunciation<br/>
_The emperor agreed to the **cession** of all colonies._
- [ ] **chasm**  深刻或裂縫 <kbd>sym</kbd> gap, cleft, cavity<br/>
_A great **chasm** appeared on the ground after the earthquake._
- [ ] **chore**  零工；雜務 <kbd>sym</kbd> light task, small job<br/>
_Feeding the chickens and milking the cows were John's **chores** on the farm._
- [ ] **chum**  室友 <kbd>sym</kbd> roommate, chamber-fellow<br/>
_He finds the new **chum** very congenial to him._
- [ ] **cluster**  串；叢 <kbd>sym</kbd> clump, bunch, group, collection<br/>
_Some flowers always grow in **clusters**._
- [ ] **clutter**  喧鬧 <kbd>sym</kbd> clatter, confused noise, disturbance<br/>
_Three minutes after the teacher left, the classroom was in a **clutter**._
- [ ] **cognizance**  認識 <kbd>sym</kbd> knowledge, cognition, knowing<br/>
_The king had **cognizance** of plots against him._
- [ ] **cohesion**  附著(力) <kbd>sym</kbd> coherence, sticking together <kbd>v.</kbd> cohere<br/>
_The **cohesion** of molecules makes the liquid form a droplet._
- [ ] **collusion**  共謀 <kbd>sym</kbd> covin, conspiracy <kbd>v.</kbd> collude<br/>
_The police and the criminals appear to be working in **collusion**._
- [ ] **colt**  小雄駒 <kbd>sym</kbd> foal<br/>
_The **colt** was a hybrid._
- [ ] **comity**  禮讓 <kbd>sym</kbd> civility, courtesy, urbanity, amenity<br/>
_It is the rule of **comity** to agree where you can._
- [ ] **compact**  契約 <kbd>sym</kbd> contract, agreement, bargain, pact, treaty, arrangement, stipulation<br/>
_They signed a **compact** to control the production of oil._
- [ ] **compassion**  憐憫 <kbd>sym</kbd> commiseration, pity, sympathy, clemency, tender-heartedness<br/>
_I ad **compassion** on him._
- [ ] **condiment**  調味品 <kbd>sym</kbd> relish, seasoning, sauce<br/>
_You've put too much **condiment** in the soup._
- [ ] **congruity**  一致 <kbd>sym</kbd> agreement, suitableness, fitness, consistency, conformity<br/>
_The **congruity** of the orchestra was disturbed by the nervous soloist._
- [ ] **connivance** 默許 <kbd>sym</kbd> tacit consent, secret agreement, secret approval<br/>
_The party took place with the **connivance** of the teacher._
- [ ] **conscience**  良心 <kbd>sym</kbd> moral sense, moral faculty, the still small voice<br/>
_The murderer had no **conscience**._
- [ ] **consensus**  一致 <kbd>sym</kbd> unanimity, agreement, concord, consent<br/>
_The **consensus** of opinions among the members is hard to achieve._
- [ ] **continence**  自制 <kbd>sym</kbd> self-command, chastity, self-control<br/>
_**Continence** is a virtue._
- [ ] **conveyance**  讓與；運輸 <kbd>sym</kbd> demise, transfer, cession, transference<br/>
_The **conveyance** of the equipment took three days._
- [ ] **cove**  小海灣 <kbd>sym</kbd> small bay or gulf<br/>
_The pirates landed at a **cove** near the town._
- [ ] **creek**  小灣；小溪 <kbd>sym</kbd> inlet, cove, bight, small river<br/>
_This **creek** abounds in trout._
- [ ] **cuisine**  烹調 <kbd>sym</kbd> cookery, style of cooking<br/>
_The **cuisine** is excellent in this hotel._
- [ ] **culmination**  頂點 <kbd>sym</kbd> acme, zenith, climax, topmost point<br/>
_He reached the **culmination** of his career after twenty years of hard working._
- [ ] **curiosity**  好奇心 <kbd>sym</kbd> inquiringness, inquisitiveness<br/>
_I bought it out of **curiosity**._
- [ ] **decadence**  衰落 <kbd>sym</kbd> decline, fall, decay, degeneracy<br/>
_The **decadence** of morals is bad for the nation._
- [ ] **declivity**  下傾的斜面 <kbd>sym</kbd> descent, slope<br/>
_A precipice is a very, very steep **declivity**._
- [ ] **default**  不履行責任；缺乏 <kbd>sym</kbd> neglect, omission, failure, lack, deficiency<br/>
_In **default** of tools, she used a hairpin and a buttonbook._
- [ ] **defection**  背叛 <kbd>sym</kbd> apostasy, revolt, desertion<br/>
_His **defection** from the party dimmed his political career._
- [ ] **deference**  敬意 <kbd>sym</kbd> esteem, respect, reverence, consideration<br/>
_Do you treat your parents and teachers with **deference**?_`
- [ ] **deity**  神 <kbd>sym</kbd> divinity, god, the Divine nature<br/>
_He despised those who worshiped the **deities**._
- [ ] **delineation**  描寫 <kbd>sym</kbd> design, sketch, outline, figure<br/>
_The author's character **delineations** are forceful._
- [ ] **demise**  讓與 <kbd>sym</kbd> conveyance, transfer, transference<br/>
_The **demise** of the old man's property caused much dispute upon his death._
- [ ] **den**  獸窟 <kbd>sym</kbd> cave, cavern<br/>
_He stepped into a tiger's **den** by mistake._
- [ ] **dent**  凹痕 <kbd>sym</kbd> dint, nick, indentation<br/>
_Bullets had made **dents** in the soldier's steel helmet._
- [ ] **deposition**  證明；革職 <kbd>sym</kbd> testimony, evidence, dismission<br/>
_The **deposition** of the last witness convicted the accused man._
- [ ] **detraction**  誹謗 <kbd>sym</kbd> depreciation, slander, slur, defamation, censure<br/>
_Happly are they that hear **detractions**, and can put them to mending._
- [ ] **detriment**  損害 <kbd>sym</kbd> damage, loss, cost, injury, mischief, harm, hurt, disadvantage<br/>
_His lack of education was a serious **detriment** to his career._
- [ ] **dictum**  格言 <kbd>sym</kbd> affirmation, saying, assertion<br/>
_It is a wise **dictum**._
- [ ] **diet**  飲食 <kbd>sym</kbd> victuals, food, nutriment, subsistence, provision<br/>
_Milk is a wholesome article of **diet**._
- [ ] **diffusion**  流布；擴散 <kbd>sym</kbd> spread, extension, dispersion, circulation<br/>
_The **diffusion** of posonous gas is hastened by the wind._
- [ ] **dilemma**  左右為難；困境 <kbd>sym</kbd> strait, quandary, perplexing alternatie, difficult choice<br/>
_Her **dilemma** was whether to go to the party in her old dress or to stay at home._
- [ ] **discretion**   斟酌；明辨 <kbd>sym</kbd> judgment, prudence, wariness<br/>
_It is within your own **discretion**._
- [ ] **dissension**  衝突；紛爭 <kbd>sym</kbd> strife, discord, variance, disagreement, contention, difference<br/>
_Political questions often cause **dissension**._
- [ ] **dissonance**  不調和 <kbd>sym</kbd> jarring, discord, discordance, cacophony, inconsistency<br/>
_**Dissonance** among the three partners doomed the project.
- [ ] **donor**  給予者 <kbd>sym</kbd> donator, bestower<br/>
_Few realized that he was the **donor** of many charities._
- [ ] **dunce**  愚蠢的人 <kbd>sym</kbd> foot, dullard, bull-head, simpleton, dolt<br/>
_The **dunce** was easily taken in._
- [ ] **dungeon**  地牢 <kbd>sym</kbd> prison, jail, underground cell<br/>
_The dethroned king was held in a **dungeon**._
- [ ] **'eclat**  榮譽；輝煌的成就 <kbd>sym</kbd> splendor, pomp, effect, briliancy, glory, luster, show<br/>
_He is a diplomat of great **'eclat**._
- [ ] **ecstasy**  恍惚；狂喜 <kbd>sym</kbd> frenzy, trance, madness, rapture, delight, joy, gladness<br/>
_He listened to the music with **ecstasy**._
- [ ] **efficacy**  功效 <kbd>sym</kbd> potency, power, vigor, efficiency, force, competency<br/>
_The medicine has no **efficacy** in the disease._
- [ ] **effigy**  芻像；雕像 <kbd>sym</kbd> figure, image, status, likeness, portrait<br/>
_The dead man's monument bore his **effigy**._
- [ ] **effrontery**  厚顏無恥 <kbd>sym</kbd> impudence, assurance, audacity, presumption, shamelessness, boldness, hardihood<br/>
_The demagogue had the *effrontery** to ask the people he had robbed to vote for him._
- [ ] **egoism**  自負 <kbd>sym</kbd> self-conceit, self-esteem, self-importance, self-assertion, self-praise<br/>
_The **egoism** of an artist sometimes seems natural._
- [ ] **egress**  出口 <kbd>sym</kbd> way out, passage out, outlet, exit<br/>
_The enemy blocked the narrow pass so that no **egress** was possible for our soldiers._
- [ ] **elation**  得意洋洋 <kbd>sym</kbd> elevation, exaltation, pride, high spirits, exultation<br/>
_Ruth was filled with **elation** at having won the prize._
- [ ] **eloquence**  雄辯 <kbd>sym</kbd> oratory, elocution, art of speaking well, impassinated utterance<br/>
_The **eloquence** of the President moved all hearts._
- [ ] **emolument**  報酬 <kbd>sym</kbd> pay, gain, renumeration, wages, salary, compensation<br/>
_The **emoluments** of this profession is not satisfactory._
- [ ] **enigma**  謎 <kbd>sym</kbd> dark saying, puzzle, obscure question<br/>
_To most of the audience, the philosopher seemed to speak in **enigma**._
- [ ] **enmity**  敵意 <kbd>sym</kbd> hatred, animosity, hostility, aversion, malevolence<br/>
_His speech is such as to incure **enmity**._
- [ ] **enmui**  怠倦 <kbd>sym</kbd> listlessness, boredom, lassitude, tiresomeness<br/>
_He is too busy to have any leisure for **ennui**._
- [ ] **enormity**  殘暴 <kbd>sym</kbd> depravity, atrocity, heinousness, nefariousness<br/>
_The **enormity** of his offense made it probable that the man was not sane._
- [ ] **entity**  存在 <kbd>sym</kbd> existence, being, essence<br/>
_In his unhappiness he had come even to question his **entity**._
- [ ] **episode**  插曲 <kbd>sym</kbd> incidental event, digression<br/>
_The year he spent in France was an important **episode** in the artist's life._
- [ ] **eulogy**  頌詞 <kbd>sym</kbd> laudation, praise, encomium, applause, commendation<br/>
_He pronounced a **eulogy** upon the hero._
- [ ] **euphony**  悅耳之音  <kbd>sym</kbd> sonance, agreeable sound<br/>
_Such **euphonies** are hard to resist._
- [ ] **evasion**  逃避 <kbd>sym</kbd> evading, subterfuge, quibble, escape<br/>
_**Evasion** of one's duty is contemptible._
- [ ] **exertion**  努力 <kbd>sym</kbd> exercise, endeavor, struggle, effort, tempt<br/>
_He failed to lift the stone in spite of all his **exertions**._
- [ ] **exodus**  大批離去 <kbd>sym</kbd> going out<br/>
_Every summer there is an **exodus** from the city._
- [ ] **exploit**  功績 <kbd>sym</kbd> feat, heroic act, deed, achievement<br/>
_Old stories tell about the **exploits** of famous heroes._
- [ ] **facet**  小平面 <kbd>sym</kbd> small surface, aspect, phase, angle, side<br/>
_It is hard to imagine how these **facets** on the diamond was made._
- [ ] **fagot**  柴把 <kbd>sym</kbd> bundle of sticks<br/>
_He built the fire with **fagots**._
- [ ] **fatalism**  宿命論 <kbd>sym</kbd> necessitarianism, submission to fate<br/>
_**Fatalism** can take the form of abject submissiveness but also of heroism._
- [ ] **fealty**  忠誠 <kbd>sym</kbd> homage, loyalty, allegiance, fidelity<br/>
_His **fealty** to his country is beyond doubt._
- [ ] **fecundity**  豐富 <kbd>sym</kbd> fertility, productivity, fruitfulness<br/>
_The boy's **fecundity** of imagination amazed his teacher._
- [ ] **feint**  假裝 <kbd>sym</kbd> trick, pretense, artifice, false appearance<br/>
_The boy made a **feint** of studying hard, though actually he was listening to the radio._
- [ ] **ferment**  騷動；激動 <kbd>sym</kbd> fermentation, agitation, rumult, commotion<br/>
_Rumores of war caused national **fermend**._
- [ ] **fetish** 神物 <kbd>sym</kbd> superstitious awe, object of superstition<br/>
_The **fetish* of the tribe was a snake carved out of stone._
- [ ] **figment**  虛構之物 <kbd>sym</kbd> fiction, invention, fabrication<br/>
_It was merely a **figment** of propaganda._
- [ ] **flaw**  裂縫；缺陷 <kbd>sym</kbd> crack, gap, defect<br/>
_A **flaw** in the dish caused it to break._
- [ ] **flexibility**  柔靭性 <kbd>sym</kbd> flexibleness, pliancy, pliability<br/>
_The **flexibility** of a man's muscles will lessen as he becomes old._
- [ ] **flora**  一切植物 <kbd>sym</kbd> plants, vegetation, botany<br/>
_He is well acquainted with the **flora** and fauna of Africa._
- [ ] **flux**  流動；變遷 <kbd>sym</kbd> flowing, transition<br/>
_All things are in a state of **flux**._
- [ ] **foliage**  葉 (集合詞) <kbd>sym</kbd> clusters of leaves, leaves, leafage<br/>
_This is a **foliage** plant._
- [ ] **ford**  淺灘 <kbd>sym</kbd> shallow crossing, wading-place<br/>
_You'll have to cross the **ford** to go to the village._
- [ ] **fray**  爭吵；衝突 <kbd>sym</kbd> battle, combat, fight<br/>
_The two gangsters seemed to be eager for the **fray**._
- [ ] **fruition**  完成；享受 <kbd>sym</kbd> fulfilment, consummation, enjoyment<br/>
_Success was the **fruition** of his years of work._
- [ ] **fusion**  融解；聯合 <kbd>sym</kbd> melting, amalgamation, commingling, blending, mingling<br/>
_A third party was formed by the **fusion** of independent Republicans and Democrats._
- [ ] **gabble**  急促而不清楚地說出 <kbd>sym</kbd> prattle, babble, small talk, idle talk<br/>
_Don't **gabble** - speak more slowly._
- [ ] **gage**  挑戰；抵押 <kbd>sym</kbd> pledge, pawn, security, defiance, challenge<br/>
_The knight left a diamond as **gage** for the horse and armor._
- [ ] **gait**  步態 <kbd>sym</kbd> carriage, walk, pace, step, manner of walk<br/>
_He has a lame **gait** because of an injured foot._
- [ ] **gale** 大風 <kbd>sym</kbd> wind, breeze, strong wind, hard blow<br/>
_Hundreds of trees were blown down in the **gale**._
- [ ] **garment**  衣服 <kbd>sym</kbd> clothes, clothing, vestment, dress<br/>
_The store sells fashionable **garments**._
- [ ] **garrulity**  饒舌 <kbd>sym</kbd> babble, talkativeness, prattle, loquaciousness<br/>
_His rough manners and **garrulity** won him attention._
- [ ] **gazette**  報紙；公報 <kbd>sym</kbd> newspaper, journal<br/>
_The firm went into the **gazette**._
- [ ] **gem**  珠寶 <kbd>sym</kbd> treasure, jewel, germ<br/>
_This is a **gem** of a poem._
- [ ] **gentility**  有教養；文雅 <kbd>sym</kbd> courtesy, good behavior <kbd>adj.</kbd> gentle<br/>
_His **gentility** is purely made up._
- [ ] **gentry**  紬士 <kbd>sym</kbd> people of good position<br/>
_The English **gentry** are next below the nobility._
- [ ] **guile**  詐術 <kbd>sym</kbd> craft, cunning, artifulness, deceit, duplicity, artifice<br/>
_A swindler uses **guile**; a robber uses force._
- [ ] **guise**  外觀；裝束 <kbd>sym</kbd> aspect, appearance, costume, form, shape, figure, garb<br/>
_He went there in the **guise** of a monk._
- [ ] **hamlet**  小村 <kbd>sym</kbd> small village, burg<br/>
_The well-known musician came from a **hamlet** in the mountain._
- [ ] **hap**  幸運；偶然發生之事 <kbd>sym</kbd> fortune, chance, luck, haphazard, event<br/>
_**Haps** and mishaps of life are unpredictable._
- [ ] **hatchet**  手斧 <kbd>sym</kbd> small ax, hack<br/>
_The two opposing countries finally decided to bury the **hatchet*._
- [ ] **heresy**  異端；邪說 <kbd>sym</kbd> error, heterodoxy, dissent<br/>
_You must not believe in such **heresies**._
- [ ] **hilarity**  歡樂 <kbd>sym</kbd> jollity, gayety, merriment, mirth, cheerfulness, glee<br/>
_Wine gives not light **hilarity**, but noisy merriment._
- [ ] **holocaust**  大屠殺 <kbd>sym</kbd> vast slaughter, immolation<br/>
_The bombardment last night was a **holocaust**._
- [ ] **homily**  講道 <kbd>sym</kbd> discourse, sermon<br/>
_The priest's **homily** was most uninspring._
- [ ] **hubbub**  嘈雜 <kbd>sym</kbd> clamor, din, outcry, confusion, disorder<br/>
_The **hubbub** of the city streets got on his nerves._
- [ ] **husbandry**  耕種；節用 <kbd>sym</kbd> agriculture, farming, cultivation, thrift, frugality<br/>
_To let the roof leak would be bad **husbandry**._
- [ ] **hybrid**  雜種；混血兒 <kbd>sym</kbd> half-breed, cross, bastard, mongrel<br/>
_The **hybrid** was tall, handsome, and intelligent._
- [ ] **ideology**  意識 <kbd>sym</kbd> sensationalism, empiricism<br/>
- [ ] **idiosyncrasy**  個人心理上的特點 <kbd>sym</kbd> idiocracy, eccentricity<br/>
_Eating no meat was Amy's **idiosyncrasy**._
- [ ] **immobility**  固定；靜止 <kbd>sym</kbd> immovableness, movelessness <kbd>adj.</kbd> immobile<br/>
- [ ] **immodesty**  無禮 <kbd>sym</kbd> indelicacy, boldness, brass, impudence<br/>
- [ ] **impetus**  推動力 <kbd>sym</kbd> force, momentum, spur<br/>
_Ambition is an **impetus** to work for success._
- [ ] **impropriety**  不適當 <kbd>sym</kbd> unsuitableness, unfitness, inappropriateness<br/>
_They were shocked by the **impropriety** of the young man's action._
- [ ] **impurity**  不純 <kbd>sym</kbd> unclearness, foulness, pollution<br/>
_Filtering the water removed some of its **impurities**._
- [ ] **incentive**  刺激；動機 <kbd>sym</kbd> inducement, incitement, stimulus<br/>
_He hasn't much **incentive** to work hard._
- [ ] **incongruity**  不和諧 (之物) <kbd>sym</kbd> unsuitableness, unfitness, incompatibility<br/>
_What **incongruity** are these?_
- [ ] **incredulity**  不信 <kbd>sym</kbd> distrust, unbelief, doubt <kbd>adj.</kbd> incredulous<br/>
- [ ] **indictment** 起訴 <kbd>sym</kbd> presentment, accusation, crimination
- [ ] **indignity**  侮辱 <kbd>sym</kbd> affront, outrage, slight, dishonor<br/>
_The bandits subjected us to all sorts of **indignities**._
- [ ] **infirmity**  虛弱 <kbd>sym</kbd> feebleness, weakness, debility, frailness<br/>
_**Infirmity** often comes with old age._
- [ ] **inkling**  暗示 <kbd>sym</kbd> intimation, hint, suggestion<br/>
- [ ] **innuendo**  暗指 <kbd>sym</kbd> insinuation, indirect, allusion, hint</>
_The gossipy old woman spread much scandal by **innuendoes**._
- [ ] **insomnia**  失眠 <kbd>sym</kbd> sleeplessness, wakefulness<br/>
_She suffered from **insomnia** whenever she was nervous._
- [ ] **intelligence**  智力 <kbd>sym</kbd> knowledge, acumen, penetration<br/>
_The boys were given an **intelligence** test._
- [ ] **intimacy**  熟悉；親近 <kbd>sym</kbd> familiarity, friendship, acquaintance <kbd>adj.</kbd> intimate<br/>
_His **intimacy** with Japan makes him the likely choice as ambassador to that country._
- [ ] **intimidation**  脅迫 <kbd>sym</kbd> threat, menace, blackmail<br/>
_He will by no means surrender to **intimidation**._
- [ ] **intuition**  直覺 <kbd>sym</kbd> insight, direct cognition
- [ ] **jug**  壺 <kbd>sym</kbd> pitcher, ewer, vessel<br/>
_A **jug** usually has a spout or a narrow neck, and a handle._
- [ ] **jurisdiction**  司法權 <kbd>sym</kbd> legal power, judicature<br/>
_A country's courts have **jurisdiction** not only over its own citizens, but also over foreigners who are living in the country._
- [ ] **ken**  視界 <kbd>sym</kbd> view, sight, field of vision<br/>
_A spire swam into my **ken**._
- [ ] **knell**  喪鐘 <kbd>sym</kbd> passingbell, deathbell, signal of death
- [ ] **labyrinth**  迷宮；錯綜複雜之事件 <kbd>sym</kbd> intricacy, maze, perplexity
- [ ] **男僕；跟班 <kbd>sym</kbd> footboy, footman
- [ ] **lagoon**  礁湖 <kbd>sym</kbd> lake
- [ ] **legion**  軍隊 <kbd>sym</kbd> army, military force, multitude, host
- [ ] **levity**  輕浮 <kbd>sym</kbd> lightness, want of weight<br/>
_Giggling in church show **levity**._
- [ ] **hode**  礦脈 <kbd>sym</kbd> metallic vein, mineral vein<br/>
_The miners struck a rich **lode** of copper._
- [ ] **malady**  疾病 <kbd>sym</kbd> disease, disorder, illlness<br/>
_It is a deadly **malady**._
- [ ] **malediction**  詛咒；誹謗 <kbd>sym</kbd> curse, denunciation<br/>
_Shakespeare's remains were guarded by a **malediction**._
- [ ] **martinet**  厲行嚴格紀律的人 <kbd>sym</kbd> formalist, precision, severe disciplinarian<br/>
_They discovered that the new teacher was a **martinet**._
- [ ] **maxim**  格言 <kbd>sym</kbd> saying, proverb, apothegm, dictum<br/>
_"Look before you leap" is a **maxim**._
- [ ] **mentor**  良師 <kbd>sym</kbd> adviser, counselor, instructor, teacher
- [ ] **misgiving**  疑懼 <kbd>sym</kbd> distrust, suspicion, doubt<br/>
_We started off through the storm with **misgivings**._
- [ ] **mishap**  不幸 <kbd>sym</kbd> mischance, misfortune, calamity, disaster<br/>
_They arrived without **mishap**._
- [ ] **moiety**  一半 <kbd>sym</kbd> half<br/>
_The site was conveyed by lease in two distinct **moieties**._
- [ ] **mote**  微塵 <kbd>sym</kbd> speck, mite, atom, spot, particle
- [ ] **nook**  角落；隱匿處 <kbd>sym</kbd> corner, recess, retired place
- [ ] **nostagia**  鄉愁 <kbd>sym</kbd> homesickness
- [ ] **novice**  新信徒；生手 <kbd>sym</kbd> proselyte, convert, tyro, beginner<br/>
_**Novices** are likely to make some mistakes._
- [ ] **obloquy** 讉責 <kbd>sym</kbd> detraction, reproach, censure, slander
- [ ] **opulence**  財富；富裕 <kbd>sym</kbd> wealth, affluence, fortune, ample means<br/>
_He owes his **opulence** to working hard._
- [ ] **oracle**  神諭 <kbd>sym</kbd> divine communication<br/>
_The ancient **oracles** were often vague and equivocal._
- [ ] **oration**  演說 <kbd>sym</kbd> speech, address, declamation, discourse
- [ ] **ordeal**  嚴酷之考驗 <kbd>sym</kbd> test, trial, touchstone
- [ ] **patricide**  弒父者 <kbd>sym</kbd> father-killer <kbd>ant.</kbd> matricide
- [ ] **penury**  貧窮 <kbd>sym</kbd> indigence, destitution, poverty<br/>
_The family lived in **penury**._
- [ ] **perjury**  偽證 <kbd>sym</kbd> false swearing
- [ ] **pinacle**  頂點 <kbd>sym</kbd> top, summit, acme, zenith<br/>
- [ ] **plague**  疫病 <kbd>sym</kbd> pset, disease, affliction, annoyance<br/>
_The **plague** is now prevailing in that city._
- [ ] **presumption**  推定；猜想 <kbd>sym</kbd> belief, opinion, guess, anticipation, assumption<br/>
_There is a strong **presumption** that he will succeed._
- [ ] **probity**  正直 <kbd>sym</kbd> integrity, honesty, faith, loyalty
- [ ] **proclivity**  傾向 <kbd>sym</kbd> tendency, inclination, propernsity
- [ ] **progenitor**  祖先 <kbd>sym</kbd> ancestor, forefather
- [ ] **progeny**  子孫 <kbd>sym</kbd> offspring, descendant
- [ ] **prognostic**  預兆 <kbd>sym</kbd> token, sign, omen, indication<br/>
_He regarded the bad weather as a **prognostic** of failure._
- [ ] **proxy**  代理人 <kbd>sym</kbd> substitute, deputy, delegate<br/>
_Can you find me a **proxy**?_
- [ ] **psyche**  精神 <kbd>sym</kbd> mind, soul
- [ ] **puddle**  泥坑 <kbd>sym</kbd> pool, muddy plash<br/>
_Hard rain leaves **puddles** in the road._
- [ ] **purport**  主旨；意義 <kbd>sym</kbd> signification, design, meaning, significance<br/>
_The **purport** of her letter was that she could not come._
- [ ] **quandary**  困惑；窘境 <kbd>sym</kbd> difficulty, puzzle, plight, perplexity, doubt, uncertainty<br/>
_I was put in a great **quandary**._
- [ ] **query**  質問；問題 <kbd>sym</kbd> question, inquiry, issue, problem<br/>
_The teacher is so ill-tempered that nobody dares to raise a **query** in his class._
- [ ] **rack**  (身體或精神上的) 巨大痛苦 <kbd>sym</kbd> torment, anguish, agony, pang, torture, extreme pain<br/>
_He was on the **rack** with a bad headache._
- [ ] **rage**  激怒；憤怒 <kbd>sym</kbd> fury, wrath, vehemence, frenzy, rampage, madness, passion<br/>
_He flew into a **rage**._
- [ ] **rump**  斜路 <kbd>sym</kbd> leap, bound, spring, sloping, incline
- [ ] **rancor**  怨恨 <kbd>sym</kbd> malignity, malevolence, malice, spite, venorn, grudge, ill-will, enmity, animosity, hate, hatred<br/>
_He has no **rancor** at heart against you._
- [ ] **ransom**  贖金；賠償金 <kbd>sym</kbd> price of redemption (explation), liberation, release<br/>
_The kidnappers asked for a **ransom** of fifty thousand dollars._
- [ ] **rapture**  狂喜 <kbd>sym</kbd> joy, delight, bliss<br/>
_He fell into **raptures** over classical music._
- [ ] **rascal**  惡棍 <kbd>sym</kbd> rogue, villain, blackgurd, knave, scalawag
- [ ] **rebate**  部份款項之退還；減少 <kbd>sym</kbd> diminution, lessening, deduction<br/>
_You are allowed a **rebate** of $10._
- [ ] **recipient**  接受者 <kbd>sym</kbd> receiver, pensioner, beneficiary, assignee<br/>
_The **recipients** of the prizes had their names printed in the paper._
- [ ] **recluse**  隱士 <kbd>sym</kbd> hermit, solitary, anchoret <kbd>compare</kbd> reclusion 隱居
- [ ] **recognition**  認識；承認 <kbd>sym</kbd> recollection, identification, acknowledgment, notice<br/>
_Few  countries gave **recognition** to the new state._
- [ ] **recollection**  記起；回想 <kbd>sym</kbd> remembrance, memory, reminiscene<br/>
_I have no **recollection** of it._
- [ ] **recourse**  求助；賴以幫助的人或物 <kbd>sym</kbd> resort, application, recurrence<br/>
_Our **recourse** in illness is to a doctor._
- [ ] **regimen**  養生之道 <kbd>sym</kbd> dieties, foot, diet<br/>
_Under such a **regimen** you'll certainly live long._
- [ ] **relapse**  復發；回復 <kbd>sym</kbd> falling back, backsliding, regress, return to a former state<br/>
_After convalescene he had a **relapse**._
- [ ] **remnant**  殘餘 <kbd>sym</kbd> remainder, residual, trace, rest, fragment
- [ ] **requital**  酬勞 <kbd>sym</kbd> reward, compensation, remuneration, payment, recompense<br/>
_What **requital** can we make for all his kindness to us?_
- [ ] **resemblance**  相似 <kbd>sym</kbd> similarity, similitude, anal, ogy, likeness, agreement<br/>
_Twins often show great **resemblance**._
- [ ] **reticence**  沈默 <kbd>sym</kbd> taciturnity, reserve<br/>
_His **reticence** are more revealing than his speech._
- [ ] **retribution**  報應 <kbd>sym</kbd> requital, repayment, reward, compensation
- [ ] **reverence**  尊敬 <kbd>sym</kbd> honor, veneration, adoration, awe <kbd>adj.</kbd> reverential <kbd>sym</kbd> respectful, reverent, submissive<br/>
_We pay **reverence** to our teachers._
- [ ] **rot**  腐爛 <kbd>sym</kbd> corruption, putrefaction, decay, decomposition<br/>
_**Rot** has set in._
- [ ] **rigor**  嚴厲 <kbd>sym</kbd> rigidity, rigidness, stiffness, hardness<br/>
_The new sheriff decided to enforce the law with **rigor**._
- [ ] **revolution**  革命 <kbd>sym</kbd> change, overturn, uprising, rebellion<br/>
_The automobile caused a **revolution** in way of traveling._
- [ ] **ruffian**  惡棍 <kbd>sym</kbd> villain, miscreant, rascal, scoundrel, robber
- [ ] **rupture**  破裂；失和 <kbd>sym</kbd> split, breach, fracture, burst, feud, quarrel<br/>
_The frontier disputes between the two countries resulted in the **rupture** of diplomatic relations._
- [ ] **sanity**  神智清楚 <kbd>sym</kbd> saneness, rationality, soundness<br/>
_**Sanity** of judgment has never deserted him._
- [ ] **sapience**  智慧 <kbd>sym</kbd> sageness, knowledge, sagacity, wisdom, intelligence
- [ ] **sarcasm**  諷刺；挖苦 <kbd>sym</kbd> jeer, gibe, taunt, satire, irony, riducule
_He was given to using **sarcasm**._
- [ ] **satire**  諷刺 <kbd>sym</kbd> sarcasm, redicule, irony, fling, lampoon
- [ ] **scandal**  醜聞 <kbd>sym</kbd> defamation, aspersion, calumny, slander<br/>
_It is a **scandal** for a city official to take tax money for his own use._
- [ ] **scatterbrain**  注意力不集中的人 <kbd>sym</kbd> thoughtless, person, giddy
- [ ] **schism**  分裂 <kbd>sym</kbd> division, split, disunion, discord, faction
- [ ] **scope**  範圍；餘裕 <kbd>sym</kbd> space, field, room, range<br/>
_Economics is a subject beyond the **scope** of a small child's mind._
- [ ] **screed**  碎片 <kbd>sym</kbd> fragment, shrewd
- [ ] **scripture**  經文 <kbd>sym</kbd> writing, document, scription, manuscript
- [ ] **scroll**  目錄；紙卷 <kbd>sym</kbd> list, schedule<br/>
_He slowly unrolled the **scroll** as he read from it._
- [ ] **scruple**  躊躇；顧忌 <kbd>sym</kbd> hesitation, hesitancy, perplexity, delicacy<br/>
_He has **scruples** about playing card games for money._
- [ ] **scrutiny**  詳審 <kbd>sym</kbd> examination, search, investigation, inquisition, inspection<br/>
_His work looks all right, but it will not bear **scruiting**._
- [ ] **scuff**  拖足而走 <kbd>sym</kbd> shuffle
- [ ] **sculpture**  雕刻 <kbd>sym</kbd> plastic art, carved work<br/>
_The marble **sculpture** is a masterpiece._
- [ ] **seclusion**  蟄居；退隱 <kbd>sym</kbd> separation, retirement, privacy, solitude<br/>
_She lives in **seclusion** apart from her friends._
- [ ] **segment**  部分；片 <kbd>sym</kbd> section, portion, division<br/>
_How many **segments** does an orange have?_
- [ ] **selfishness**  自私 <kbd>sym</kbd> egoism, private, interest, self-love<br/>
_His **selfishness** made him unpopular._
- [ ] **semblance**  類似；外觀 <kbd>sym</kbd> likeness, resemblance, appearance, aspect<br/>
_He bears the **semblance** of an angel and the heart of a devil._
- [ ] **sentry**  守望；哨兵 <kbd>sym</kbd> watch, guard, sentinel, watchman, patrol<br/>
_We stood **sentry** over the sleepers._
- [ ] **serenity**  安靜；從容 <kbd>sym</kbd> calmness, peacefulness, collectedness, tranquility<br/>
_The old man died with **serenity**._
- [ ] **shade**  蔭；陰暗 <kbd>sym</kbd> shadow, umbrage, darkness, dusk, obscurity<br/>
_There is not much **shade** there._
- [ ] **siege**  圍困；圍攻 <kbd>sym</kbd> investment, beleaguerment, blockade
- [ ] **slag**  渣滓 <kbd>sym</kbd> refuse, dross, cinder, clinker<br/>
_There is nothing like a brisk walk to drag the **slag** from your head._
- [ ] **slap**  摑 <kbd>sym</kbd> blow, clap<br/>
_He got a **slap** in the face._
- [ ] **snarl**  混亂 <kbd>sym</kbd> entanglement, tangle, complication, intricacy<br/>
_His legal affairs are in a **snarl**._
- [ ] **spike**  大釘 <kbd>sym</kbd> large nail, pin<br/>
_The ballplayers wear shoes with **spikes**._
- [ ] **spine**  脊骨 <kbd>sym</kbd> thorn, barb, backbone, column
- [ ] **splash**  污跡 <kbd>sym</kbd> blot, daub, spot<br/>
_She has **splashes** of grease on her dress._
- [ ] **splendor**  光彩；壯觀 <kbd>sym</kbd> brilliance, luster, brightness, radiance<br/>
_He was inspired by the **splendor** of the scene._
- [ ] **spouse**  配偶 <kbd>sym</kbd> married person, partner, companion<br/>
_Mr. Smith is Mrs. Smith's **spouse**, and she is his **spouse**._
- [ ] **spray**  小枝；水沬 <kbd>sym</kbd> shoot, twig, branch, bought, frath, foam, spume<br/>
_We were wet with the sea **spray**._
- [ ] **squad**  小隊；班 <kbd>sym</kbd> hand, gang, relay
- [ ] **stall**  廄；攤 <kbd>sym</kbd> stable, compartment, cell, recess
- [ ] **stamina**  體力；精力 <kbd>sym</kbd> firm parts, strength, vigor, force
- [ ] **staple**  主要產品；纖維；原料 <kbd>sym</kbd> commodity, fiber, filament, raw material<br/>
_Sugar is the **staple** of Taiwan._
- [ ] **statue**  像 <kbd>sym</kbd> image<br/>
_The **Statue** of Liberty is in New York Bay._
- [ ] **strain**  緊張；張力 <kbd>sym</kbd> extreme tension, stress, over-exertion<br/>
_Engineers calculate the **strains** and stresses on a bridge._
- [ ] **strait**  海峽；困難 <kbd>sym</kbd> narrow pass, gut, distress, difficulty<br/>
_The man was in great **straits**.
- [ ] **strap** 皮帶 <kbd>sym</kbd> thong, razor-strap<br/>
_The box was strengthened by **straps** of steel._
- [ ] **streak**  條紋；短時間 <kbd>sym</kbd> ray, line, run<br/>
_He has a **streak** of dirt on his face._
- [ ] **submission**  屈服；服從 <kbd>sym</kbd> surrender, cession, yielding, obedience, resignation<br/>
_The defeated general showed his **submission** by giving up his sword._
- [ ] **suffrage**  投票；同意 <kbd>sym</kbd> vote, ballot, approval<br/>
_The voters gave their **suffrage** to him._
- [ ] **suspense**  焦慮 <kbd>sym</kbd> uncertainty, indetermination<br/>
_We waited in great **suspense** for the doctor's opinion._
- [ ] **sustenance**  生計；營養物 <kbd>sym</kbd> subsistence, maintenance, victuals<br/>
_He has gone for a week without **sustenance**._
- [ ] **swarm**  群 <kbd>sym</kbd> multitude, crowd, mass, press, flock<br/>
_**Swarms** of children were playing in the park._
- [ ] **tact**  老練；機智 <kbd>sym</kbd> adoitness, discernment, cleverness<br/>
_An ambassador must have great **tact**._
- [ ] **tactics**  戰術 <kbd>sym</kbd> strategy, art of way, policy<br/>
_The book says that **tactics** differs from strategy._
- [ ] **tenement**  家屋；住宅 <kbd>sym</kbd> dwelling, habitation, abode, domicile, house<br/>
_A two-family house has two **tenements**._
- [ ] **terror**  恐怖 <kbd>sym</kbd> fright, alarm, horror, fear, dread<br/>
_His **terror** was so great that he could do nothing._
- [ ] **testimony**  證語 <kbd>sym</kbd> affirmation, deposition, attestation, confirmation<br/>
_A witness gave **testimony** that the accused was drunk._
- [ ] **thrall** 奴隷 <kbd>sym</kbd> slave, boundman, serf<br/>
_He is a **thrall** to drink._
- [ ] **throng** 群眾 <kbd>sym</kbd> crowd, horde, swarm, mob<br/>
_There were a **throng** of people in front of the police station._
- [ ] **thorment**  痛苦 <kbd>sym</kbd> anguish, agony, torture, pang, extreme pain<br/>
_My wife is a real **torment** to me._
- [ ] **trash**  廢物 <kbd>sym</kbd> dross, rubbish, waste, matter, refuse<br/>
_That book is mere **trash**._
- [ ] **travesty**  滑稽化；諧謔化 <kbd>sym</kbd> caricature, imitation, parody
- [ ] **tremor**  顫抖 <kbd>sym</kbd> shaking, trembling, quivering, trepidation<br/>
_There was a **tremor** in his voice._
- [ ] **trend**  傾向 <kbd>sym</kbd> tendency, drift, bent, direction<br/>
_The hills have a western **trend**._
- [ ] **trial**  審判 <kbd>sym</kbd> testing, test, hearing<br/>
_He was on **trial** for theft._
- [ ] **tribulation**  苦難 <kbd>sym</kbd> distress, suffering, trouble, grief<br/>
_War is a time of **tribulation**._
- [ ] **tumult**  騷動 <kbd>sym</kbd> affray, uproar, feud, row, altercation<br/>
_The fire caused a **tumult** in the theater._
- [ ] **turmoil**  騷動；混亂 <kbd>sym</kbd> tumult, disturbance, disorder, agitation<br/>
_Six robberies in one night put our village in a **turmoil**._
- [ ] **twig**  小枝 <kbd>sym</kbd> shoot, spray, branch, stem
- [ ] **vacillation**  優柔寡斷；動搖 <kbd>sym</kbd> swaying, reeling, wavering, fluctuation<br/>
_His constant **vacillation** made him an unfit administrator._
- [ ] **valor**  勇氣 <kbd>sym</kbd> bravery, courage, boldness, gallantry
- [ ] **vanity**  空虛 <kbd>sym</kbd> emptiness, hollowness<br/>
_All is **vanity**._
- [ ] **variation** 變化 <kbd>sym</kbd> alteration, mutation, modification, deviation<br/>
_The **variation** of temperature was 30 degrees yesterday._
- [ ] **vault**  拱形圓屋頂；地窖 <kbd>sym</kbd> arched ceiling, cellar<br/>
_The hotel has a wine **vault**._
- [ ] **vehemence**  熱切；激烈 <kbd>sym</kbd> ardor, fervor, impetuosity
- [ ] **venom**  毒液；惡意 <kbd>sym</kbd> poison, malignity, spite<br/>
_Her enmies dreaded the **venom** of her tongue._
- [ ] **ventilation**  通風 <kbd>sym</kbd> airing, fanning<br/>
_The **ventilation** should be improved._
- [ ] **veracity**  真實性 <kbd>sym</kbd> truthfulness, ingenuousness, sincerity<br/>
_The **veracity** of the statement is doubtful._
- [ ] **vertigo**  眩暈 <kbd>sym</kbd> dizzleness, giddiness
- [ ] **vigor**  精力 <kbd>sym</kbd> strength, force, power, activity<br/>
_He is full of **vigor**._
- [ ] **vivacity**   快活 <kbd>sym</kbd> animation, life, activity
- [ ] **vulgarity**  粗俗 <kbd>sym</kbd> crossness, rudeness, meanness<br/>
_Spitting in public or chewing gum at a dance is a sign of **vulgarity**._
- [ ] **wail**  哭泣 <kbd>sym</kbd> moan, lamentation, complaint, plaint<br/>
_She was **wailing** for her lost child._
- [ ] **warrant**  保證；權限 <kbd>sym</kbd> guarantee, surety, authority<br/>
_You have no **warrant** for doing so._
- [ ] **waver**  擺動；動搖 <kbd>sym</kbd> vacillation, fluctuation<br/>
_The battle line **wavered** and broke._
- [ ] **whim**  奇想 <kbd>sym</kbd> freak, fancy, humor, wish<br/>
_She has a **whim** for gardening, but it won't last long._
- [ ] **wrangler**  爭吵者 <kbd>sym</kbd> disputant, quarrelsome, fellow
- [ ] **zenith**  頂點；全盛 <kbd>sym</kbd> summit, apex, pinnacle, top<br/>
_He was at the **zenith** of his fortunes._