- [ ] **abhorrent**  嫌惡的 <kbd>sym</kbd> hating, abominating, detest, loathing, offensive<br/>
_I am **abhorrent** of it._
- [ ] **abject**  卑鄙的 <kbd>sym</kbd> vile, base, mean, low, despicable, contemptible, dirty, worthless, ignoble, pitiful<br/>
_He is an **abject** liar._
- [ ] **abortive**  失敗的 <kbd>sym</kbd> failing, untimely, incomplete, unavailing<br/>
_The early attempts to make airplanes were **abortive**._
- [ ] **absurd**  荒謬的 <kbd>sym</kbd> unreasonable, irrational, nonsensical, foolish, senseless, silly, stupid<br/>
_Their request is **absurd**._
- [ ] **accidental**  偶然的 <kbd>sym</kbd> casual, fortuitous, contingent, happening by chance, incidental, adventitious<br/>
_Our meeting was quite **accidental**._
- [ ] **acrid**  辛辣的 <kbd>sym</kbd> biting, sharp, pungent, burning, bitter<br/>
_Smoke feels **acrid** in your mouth and nose._
- [ ] **addle**  昏亂的；腐壞的 <kbd>sym</kbd> spoiled, barren, unfruitful, fruitless, abortive, unproductive<br/>
_I wish him an ounce more wit in his **addle** head._
- [ ] **adamant**  堅定不移的 <kbd>sym</kbd> very hard, unyielding<br/>
_He is **adamant** to temptations._
- [ ] **adjacent**  鄰接的 <kbd>sym</kbd> adjoining, neighboring, near, close<br/>
_The house **adjacent** to ours has been sold._
- [ ] **adroit**  機巧的 <kbd>sym</kbd> deterous, skilful, apt, handy, ready, clever<br/>
_He is **adroit** in handling machinery._
- [ ] **adventitious**  偶發的 <kbd>sym</kbd> accidental, incidental, fortuitous, foreign<br/>
_In works of imagination and sentiment meter is but **adventitious** to composition._
- [ ] **adverse**  逆的；反對的 <kbd>sym</kbd> opposing, counteracting, contray, conflicting<br/>
_It was **adverse** to his interests to lend me the money._
- [ ] **aesthetic**  美的 <kbd>sym</kbd> tasteful, gratifying, becoming fit
- [ ] **afflicting**  痛苦的 <kbd>sym</kbd> painful, troublous, tormenting, grievous, harassing
- [ ] **aglow**  發紅的 <kbd>sym</kbd> shining, brightly, glowing<br/>
_The sky is **aglow** with sunset colors._
- [ ] **ambiguous**  含糊的 <kbd>sym</kbd> vague, equivocal, obscure<br/>
_This sentence is **ambiguous** in sense._
- [ ] **amenable**  有責任的 <kbd>sym</kbd> liable, accountable, answerable, responsible<br/>
_We are all **amenable** to the law._
- [ ] **amiable**  友善的；和藹的 <kbd>sym</kbd> lovable, lovely, sweet, charming<br/>
_She has an **amiable** disposition._
- [ ] **amicable**  和平的；和睦的 <kbd>sym</kbd> harmonious, friendly, kind, cordial<br/>
_When nations cannot settle a quarrel in an **amicable** way, they usually go to war._
- [ ] **ample**  充足的 <kbd>sym</kbd> great, large wide, capacious, extensive<br/>
_They have an ample supply of rice._
- [ ] **animated**  有生氣的；熱烈的 <kbd>sym</kbd> lively, vigorous, brisk, spirited, blithe, gay, cheerful
- [ ] **animating**  鼔舞生氣的；啟發的 <kbd>sym</kbd> cheering, inspiring, enliving<br/>
_The talk was incessant and **animating**._
- [ ] **anomalous**  反常的 <kbd>sym</kbd> abnormal, irregular, unusual<br/>
_A position as head of a department, but with no real authority, is **anomalous**._
- [ ] **antique**  古代的 <kbd>sym</kbd> ancient, old, archaic<br/>
_This **antique** chair was made in 1750._
- [ ] **apathetic**  冷淡的 <kbd>sym</kbd> passionless, unfeeling, inert, passive, stoical, insensible<br/>
_Not that the people were **apathetic**, for they were curious about everything._
- [ ] **appropriate**  適合的 <kbd>sym</kbd> adapted, fitting, proper<br/>
_Plain clothes are **appropriate** for school wear._
- [ ] **approximate**  近似的 <kbd>sym</kbd> coming near, proximate, approaching<br/>
_The **approximate** population of China is 800 millions._
- [ ] **archaic** 古的 <kbd>sym</kbd> old, ancient, antiquated
- [ ] **arduous**  峻險的；困難的 <kbd>sym</kbd> steep, lofty, uphill, hard, laborious, onerous<br/>
_This is an **arduous** enterprise._
- [ ] **assiduous**  勤勉的 <kbd>sym</kbd> diligent, industrious, arduous
- [ ] **audacious**  大膽的 <kbd>sym</kbd> daring, bold, fearless, courageious, intrepid, venturesome
- [ ] **avid**  貪婪的 <kbd>sym</kbd> eager, greedy<br/>
_The younger generation is **avid** of new experience._
- [ ] **azure** 蔚藍的 <kbd>sym</kbd> blue, sky-blue
- [ ] **bald**   秃頭的；光秃的 <kbd>sym</kbd> bare, hairless, destitute of hair<br/>
_The mountain became **bald** after the fire._
- [ ] **balmy**  安慰的；芳香的；溫和的 <kbd>sym</kbd> soothing, refreshing, easing, fragrant, odoriferous
- [ ] **belligerent**  好戰的；交戰的 <kbd>sym</kbd> warlike, hostile, carrying on war, bellicose, pugnacious, quarrelsome
- [ ] **beneficial**  有益的 <kbd>sym</kbd> useful, profitable, helpful, favorable<br/>
_Exercise is **beneficial** to the health._
- [ ] **bleak**  寒冷的 <kbd>sym</kbd> cold, chill, chilly, biting, piercing
- [ ] **brittle**  易碎的 <kbd>sym</kbd> crisp, fragile, frail<br/>
_Glass is **brittle**._
- [ ] **brisk**  活潑的 <kbd>sym</kbd> active, lively, quick, agile, amart, alert, animated<br/>
_He is a **brisk** walker._
- [ ] **brutal**  野蠻的 <kbd>sym</kbd> savage, ferocious, cruel, inhuman, unfeeling, barbarian
- [ ] **callous**   無感覺的 <kbd>sym</kbd> insensible, unfeeling, apathetic, obtuse, dull, torpid<br/>
_He is **callous** to rebuke._
- [ ] **capacious**  能容大量的 <kbd>sym</kbd> ample, spacious, large, broad, extensive, roomy
- [ ] **capricious**  任性的 <kbd>sym</kbd> whimsical, fanciful, wanton, fickle<br/>
_A spoiled child is often **capricious**._
- [ ] **captious**  好吹毛求疵的 <kbd>sym</kbd> censoring, carping, caviling<br/>
- [ ] **cardinal**  首要的；基本的 <kbd>sym</kbd> chief, principle, fundamental<br/>
_This is a matter of **cardinal** significance._
- [ ] **carnal**  肉體的 <kbd>sym</kbd> sensual, flelshy<br/>
_Gluttony and drunkeness have been called **carnal** vices._
- [ ] **carping**  吹毛求疵的 <kbd>sym</kbd> caviling, censorious
- [ ] **chaste**  貞節的；純正的 <kbd>sym</kbd> pure, virtuous, pure-minded, modest
- [ ] **chilly**  寒冷的 <kbd>sym</kbd> cool, cold<br/>
_It was rather **chilly** that day._
- [ ] **clamorous**  吵鬧的 <kbd>sym</kbd> vociferous, noisy, boisterous
- [ ] **clandestine**  秘密的 <kbd>sym</kbd> secret, underhand, furtive<br/>
_They planned a **clandestine** marriage._
- [ ] **cogent**  強有力的 <kbd>sym</kbd> forcible, effective, powerful, urgent<br/>
_The lawyer's **cogent** arguments covinced the jury._
- [ ] **complicmentary**  讚美的 <kbd>sym</kbd> laudatory, flattering
- [ ] **confounded**  驚惶失措的；困惑的 <kbd>sym</kbd> miserable, shameful, horrid
- [ ] **congenial**  意氣相投的 <kbd>sym</kbd> kindred, similar, sympathetic<br/>
_In this small village, he found few persons **congenial** to him._
- [ ] **congenital**  先天的 <kbd>sym</kbd> connate, inborn, existing from birth
- [ ] **conscientious**  正直的 <kbd>sym</kbd> honest, just, honorable<br/>
_He is a **conscientious** judge._
- [ ] **conspicuous**  引人注目的 <kbd>sym</kbd> visible, perceptible, apparent, noticeable<br/>
_She is a **conspicuous** figure._
- [ ] **conventional**  傳統的；習俗的 <kbd>sym</kbd> usual, customary, orthodox, traditional<br/>
_"Good morning" and "Good evening" are **conventional** greetings._
- [ ] **covetous**  貪心的 <kbd>sym</kbd> desirous, avaricious<br/>
_They threw **covetous** eyes out of their forest on the fields and vineyards of their neighbor._
- [ ] **cozy**  舒適的 <kbd>sym</kbd> confortable, easy
- [ ] **crass**  愚蠢的；厚而粗的 <kbd>sym</kbd> cross, thick, unrefined, raw
- [ ] **crisp**  不高興的 <kbd>sym</kbd> fretful, peevish, petulant, waspish<br/>
_Why are you so **cross** with me?_
- [ ] **crude**  未提練的；生的 <kbd>sym</kbd> rough, raw, undressed, crass, unpolished, rude
- [ ] **cursory**  匆促的 <kbd>sym</kbd> hasty, slight, careless, passing, desultory<br/>
_Even a **cursory** reading of the letter showed many errors._
- [ ] **cynical**  譏刺的 <kbd>sym</kbd> carping, censorious, sarcastic, captious
- [ ] **dank**  陰濕的 <kbd>sym</kbd> moist, damp, humid, wet<br/>
_The cave was dark, **dank** and chilly._
- [ ] **dauntless**  勇敢的 <kbd>sym</kbd> indomitable, unconquerable, bold, daring<br/>
_He is a **dauntless** aviator._
- [ ] **deceptive**  虛偽的；騙人的 <kbd>sym</kbd> deceitful, deceiving, delusive, misleading<br/>
_Appearances are often **deceptive**._
- [ ] **deciduous**  每年落葉的 <kbd>sym</kbd> non-perennial, caducous<br/>
_The maple is a **deciduous** tree._
- [ ] **defiant**  大膽反抗的 <kbd>sym</kbd> resistant, contumacious, bold, daring, courageious<br/>
_He assumed a **defiant** attitude toward his employer._
- [ ] **decrepit**  衰老的 <kbd>sym</kbd> effete, shattered, infirm with age
- [ ] **defunct**  死的 <kbd>sym</kbd> dead, departed, gone
- [ ] **dejected**  失望的；沮喪的 <kbd>sym</kbd> disheartened, dispirited, discouraged, depressed<br/>
_The **dejected** expression on the face of the loser spoiled my victory._
- [ ] **deleterious**  有害的 <kbd>sym</kbd> destructive, deadly, noxious<br/>
_This food is **deleterious** to health._
- [ ] **detrimental**  有害的 <kbd>sym</kbd> harmful, hurtful<br/>
_Lack of sleep is **detrimenteal** to one's health._
- [ ] **devious**  繞道的 <kbd>sym</kbd> deviating, erratic, wandering<br/>
_We too the **devious** route home to avoid the crowds in the main roads._
- [ ] **devoid**  缺乏的 <kbd>sym</kbd> vacant, empty, bare, destitute<br/>
_A well **devoid** of water is useless._
- [ ] **devout**  虔誠的 <kbd>sym</kbd> religious, pious, holy, devotional
- [ ] **dexerous**  機巧的 <kbd>sym</kbd> skilful, adroit, adept, handy, ready, expert<br/>
_She is a **dexterous** typist._
- [ ] **diminutive**  小的 <kbd>sym</kbd> smal, little tiny
- [ ] **dingy**  昏暗的；骯髒的 <kbd>sym</kbd> dusky, dun, soiled, sulled, dulled, dirty, shabby
- [ ] **discreet**  慎重的；謹慎的 <kbd>sym</kbd> prudent, judicious, cautious, considerate, wary<br/>
_He is **discreet** inchoosing his friends._
- [ ] **discrete**  分立的 <kbd>sym</kbd> separate, distinct, discontinous, discretive
- [ ] **dismal**  陰暗的 <kbd>sym</kbd> gloomy, dreary, oppressive<br/>
_What **dismal** weather!_
- [ ] **docile** 聽話的；溫順的 <kbd>sym</kbd> teachable, tracetable, pliant, yielding
- [ ] **dormant**  蟄伏的 <kbd>sym</kbd> sleeping, at rest, latent, quiescent<br/>
_Many animals are in a **dormant** state during winter._
- [ ] **dorsal**  背部的 <kbd>sym</kbd> tergal, on the back<br/>
_The **dorsal** fin of a shark is its characteristic._
- [ ] **downcast**  沮喪的 <kbd>sym</kbd> sad, dejected, dispirited, despondent<br/>
_A life of failure had made her **downcast**._
- [ ] **drowsy**  昏昏欲睡的 <kbd>sym</kbd> sleepy, dozy, lulling<br/>
_It was a warm, quiet, **ddrowsy** afternoon._
- [ ] **ductile**  柔軟的；馴良的 <kbd>sym</kbd> tractable, compliant, yielding, pliant<br/>
_Wax is **ductile**._
- [ ] **eccentric**  古怪的 <kbd>sym</kbd> parabolic, abnormal, peculiar, unnatural<br/>
_The **eccentric** person was avoided by others._
- [ ] **edible**  可食的 <kbd>sym</kbd> eatable, esculent<br/>
_Toadstools are not **edible**._
- [ ] **elusive**  躲避的 <kbd>sym</kbd> evasive, shuffling
- [ ] **eminent**  聞名的 <kbd>sym</kbd> lofty, distinguished, conspicuous, prominent<br/>
_He is **eminent** as a painter._
- [ ] **equitable**  公平的；公正的 <kbd>sym</kbd> upright, just, honest, reasonable<br/>
_Paying a person what he has earned is **equitable**._
- [ ] **erratic**  不穩定的 <kbd>sym</kbd> wandering, roving<br/>
_An **erratic** mind jumps from one idea to another._
- [ ] **erroneous**  錯誤的 <kbd>sym</kbd> , incorrect<br/>
_That the earth is flat is an **erroneous** concept._
- [ ] **erudite**  博學的 <kbd>sym</kbd> learned, lettered, well-read, scholarly
- [ ] **essential**  重要的；基本的 <kbd>sym</kbd> vital, highly important, fundamental<br/>
_Love of fair play is said to be an **essential** part of the English character._
- [ ] **estimable**  可估計的 <kbd>sym</kbd> appreciable, computable
- [ ] **ethnic**  種族的 <kbd>sym</kbd> relating to race, racial
- [ ] **excitable**  易激動的 <kbd>sym</kbd> sensitive, impressible, irascible<br/>
_Prima donnas have the reputation of being **excitable** and temperamental._
- [ ] **expedient**  權宜的；方便的 <kbd>sym</kbd> fit, proper, suitable, desirable<br/>
_Morality, for him, means doing what is **expedient**._
- [ ] **expeditious**  敏捷的；迅速的 <kbd>sym</kbd> speedy, swift, rapid
- [ ] **exquisite** 精美的；靈敏的 <kbd>sym</kbd> nice, exact, discrimiating, appreciative<br/>
_Those violets are **exquisite** flowers._
- [ ] **extant**  現存的 <kbd>sym</kbd> existing, in being, in existence<br/>
_Some of Washington's letters are **extant**._
- [ ] **extensive**  大量的；廣泛的 <kbd>sym</kbd> large, wide, broad, capacious<br/>
_**Extensive** funds will be needed._
- [ ] **exultant**  歡騰的；狂歡的 <kbd>sym</kbd> exulting, joyous, transported, elated <kbd>v.</kbd> exult<br/>
_He gave an **exultant** shout._
- [ ] **facetious**  好開玩笑的；滑稽的 <kbd>sym</kbd> jocular, witty, jocase, humorous, funny<br/>
_He was so **facetious** that he turned everything into a joke._
- [ ] **facile**  輕而易舉的；隨和的 <kbd>sym</kbd> mild, easy, affable, pliant, pliable, flexible<br/>
_Her **facile** nature adapted itself to any company._
- [ ] **factious**  好植黨派的 <kbd>sym</kbd> trubulent, seditious, rebellious, full of faaction<br/>
_Many of the old puritan colonists retained their **factious** temperaments in the New World._
- [ ] **factitious**  人為的；不自然的 <kbd>sym</kbd> artificial, not-natured<br/>
_Extensive advertising can cause a **factitious** demand for an article._
- [ ] **fallacious**  欺騙的 <kbd>sym</kbd> misleading, delusive
- [ ] **fallow**  休耕的 <kbd>sym</kbd> uncultivated, idle
- [ ] **famished**  極饑餓的 <kbd>sym</kbd> hungry, starved, ravenous<br/>
_The child looked half **famished**._
- [ ] **fatuous**  愚昧的 <kbd>sym</kbd> foolish, stupid, mad, senseless
- [ ] **feeble**  微弱的 <kbd>sym</kbd> weak, debilitated, sickyly<br/>
_His pulse was very **feeble**._
- [ ] **fell**  殘忍的 <kbd>sym</kbd> cruel, inhuman, relentless, pitiless
- [ ] **ferocious**  兇猛的 <kbd>sym</kbd> untamed, savage, cruel, inhuman, fell, fierce<br/>
_The gunster had a **ferocious** look._
- [ ] **fervent**  白熱的；強烈的 <kbd>sym</kbd> glowing, hot, buring, melting, ardent, fiery<br/>
_His **fervent** hatred was expressed in his speech._
- [ ] **fervid**  熱情的；灼熱的 <kbd>sym</kbd> fervent, hot, burning, fiery, boiling<br/>
_We will set out when the **fervid** heat subsides._
- [ ] **fickle**  多變的 <kbd>sym</kbd> inconstant, waving, faithless, unsteady, unstable<br/>
_People are very easily misled and very **fickle** in their views._
- [ ] **flaccid**  軟弱的；鬆弛的 <kbd>sym</kbd> soft, limber, llimp, drooping, flabby, yielding
- [ ] **fitful**  一陣陣的；斷續的 <kbd>sym</kbd> variable, irregular, intermittent<br/>
_A **fitful** noise interrupted his sleep._
- [ ] **flimsy**  脆弱的；薄弱的 <kbd>sym</kbd> slight, thin, weak, feeble, trivial<br/>
_Tissue paper is **flimsy**._
- [ ] **florid**  顏色鮮麗的；鮮紅的 <kbd>sym</kbd> flowery, rosy
- [ ] **fluffy**  絨毛的；毛茸茸的 <kbd>sym</kbd> nappy, downy, flossy<br/>
_Newly hatched chickens are like **fluffy** balls._
- [ ] **forcible**  有力的；強行的 <kbd>sym</kbd> powerful, strong, potent, violent, all-powered, compulsory, coercive<br/>
_He is a **forcible** speaker who can easily get his ideas across to the audience._
- [ ] **fractious**  易怒的；脾氣壞的 <kbd>sym</kbd> cross, captious, petulant, touchy, tasty, peevish<br/>
- [ ] **frantic**  狂亂的 <kbd>sym</kbd> furious, raving, mad, distracted
_Don't drive him **frantic** by worrying him with endless questions._
- [ ] **fraudulent**  詐欺的；不誠實的 <kbd>sym</kbd> dishonest, deceitful, deceptive
- [ ] **frigid**  嚴寒的 <kbd>sym</kbd> cold, cool
_The **frigid** weather kept most people indoors._
- [ ] **frowzy**  不整潔的；臭的 <kbd>sym</kbd> fetid, rank, noisome, rancid
- [ ] **fuddled**  醉的 <kbd>sym</kbd> drunk, muddled, intoxicated
- [ ] **fulsome**  可厭的；令人作嘔的 <kbd>sym</kbd> gross, offensive, excessive
- [ ] **funereal**  葬禮的；憂鬱的 <kbd>sym</kbd> mournful, sad, lugubrious, melancholy<br/>
_She has a **funereal** expression on her face._
- [ ] **garrulous**  愛說話的 <kbd>sym</kbd> loquacious, talkative, prating<br/>
_The **garrulous** old man often bored his audience._
- [ ] **gauche**  笨拙的；粗魯的 <kbd>sym</kbd> clumsy, impolite, awkward<br/>
_No one liked her shy, tacitum, and **gauche** manner._
- [ ] **germane**  有關係的；適切的 <kbd>sym</kbd> related, akin, allied, pertinent, apposite<br/>
_The statement was **germane** to the argument._
- [ ] **glassy**  如鏡的；透明的 <kbd>sym</kbd> crystal, hyaline, lucent
- [ ] **glib**  口齒伶俐的 <kbd>sym</kbd> smmoth, alippery, voluble, fluent<br/>
_A **glib** salesman sold her a set of dishes that she didn't want._
- [ ] **gloomy**  暗的；憂悶的 <kbd>sym</kbd> dejected, sad, depressed, disheartened<br/>
_The college graduate felt **gloomy** about his future._
- [ ] **greedy**  貪婪的 <kbd>sym</kbd> voracious, ravenous, gluttonous, devouring<br/>
_The child is looking at the cakes with **greedy** eyes._
- [ ] **grouchy**  不悅的；慍怒的 <kbd>sym</kbd> sullen, sulky, morose, moody
- [ ] **grim**  冷酷的；可怕的 <kbd>sym</kbd> rigorous, hard, eruel<br/>
_He made **grim** jokes about death and ghosts._
- [ ] **gruff**  粗暴的；粗野的 <kbd>sym</kbd> rough, rude, uncourteous, ungracious, impolite, bluff, harsh, blunt, churish
- [ ] **grumpy**  性情暴躁的 <kbd>sym</kbd> sullen, sour, grum, surly
- [ ] **haphazard**  偶然的；隨便的 <kbd>sym</kbd> random, without, purpose, aimless<br/>
_**Haphazard** answers are usually wrong._
- [ ] **hazy**  有霧的；模糊的 <kbd>sym</kbd> foggy, misty, uncertain<br/>
_I am rather **hazy** as to waht your purpose is._
- [ ] **hectic**  發熱的；緊張興奮的 <kbd>sym</kbd> constitutional, feverish, hot, heated<br/>
_The journalist leads a **hectic** life._
- [ ] **hilarious**  高興的；熱鬧的 <kbd>sym</kbd> jovial, gay, jolly, merry, boisterous
- [ ] **hirsute**  多毛的 <kbd>sym</kbd> hairy, shaggy, rough, rude
- [ ] **idiotic**  愚蠢的 <kbd>sym</kbd> sottish, foolish, fatuous<br/>
_The child's **idiotic** deeds caused his family much trouble._
- [ ] **illegible**  難讀的 <kbd>sym</kbd> unreadable<br/>
_The writer's manuscript was **illegible**._
- [ ] **illicit **違法的** <kbd>sym</kbd> illegal, illegitimate, unalwful
- [ ] **impartial**  公平的 <kbd>sym</kbd> unbiased, unprejudiced, candid<br/>
_Law shall be uniform and **impartial**.
- [ ] **impassible**  無知覺的 <kbd>sym</kbd> insensible, indifferent, insusceptible<br/>
_He was **impassible** before victory, before danger, before defeat._
- [ ] **impeccable**  無暇疵的 <kbd>sym</kbd> faultless, sinless, stainless
- [ ] **imperative**  急需的；命令的 <kbd>sym</kbd> commanding, urgent<br/>
_It is **imperative** for us to have a strong air force._
- [ ] **impertinent**  粗魯的 <kbd>sym</kbd> uneasy, unquiet, rude<br/>
_Your rash, **impertinent** remarks get you into trouble._
- [ ] **impetuous**  猛烈的；衝動的 <kbd>sym</kbd> vehement, violent, furious, ardent, passionate<br/>
_Children are always more **impetuous** than old people._
- [ ] **impious**  不虔敬的 <kbd>sym</kbd> irreverent, irreligious, wicked
- [ ] **impromptu**  臨時的 <kbd>sym</kbd> extempore, off-hand, improvised<br/>
_He is good at making **impromptu** speeches._
- [ ] **imcipient**  初期的 <kbd>sym</kbd> beginning, inchoate<br/>
_The medicine stopped David's incipient cold._
- [ ] **incoherent**  不連貫的 <kbd>sym</kbd> loose, non-adhesive, inconsistent<br/>
_He was so drunk that he was quite **incoherent**._
- [ ] **indefatigable**  不疲倦的 <kbd>sym</kbd> untiring, unwearied, tireless, unflagging
- [ ] **ineligible**  無資格的；不適當的 <kbd>sym</kbd> disqualified, unadvisable, objectionable<br/>
_A foreign-born citizen is **ineligible** for the Presedency._
- [ ] **inept**  不適宜的 <kbd>sym</kbd> inappropriate, unsuitable
- [ ] **insidious**  狡猾的 <kbd>sym</kbd> artful, wily, guileful, crafty
- [ ] **intense**  非常的；緊張的 <kbd>sym</kbd> strained, strict, intent, extreme, close, severe<br/>
_The atmosphere in the meeting was **intense**._
- [ ] **internal**  內在的 <kbd>sym</kbd> inner, interior, inside<br/>
_He suffered **internal**  injuries in the accident._
- [ ] **intricate**  錯綜複雜的；難懂的 <kbd>sym</kbd> entangled, complicated, perplexed, mazy
- [ ] **intrinsic**  本質的；本身的 <kbd>sym</kbd> inherent, internal, inborn, inbred, natural<br/>
_The **intrinsic** of a dollar bill is only that of a piece of paper._
- [ ] **jocose**  滑稽的 <kbd>sym</kbd> jocular, humorous, witty, jesting, facetious
- [ ] **jovial**  快活的 <kbd>sym</kbd> merry, joyous, gay, convivial, jolly<br/>
_I am in a **jovial** mood today._
- [ ] **lank**  細長的 <kbd>sym</kbd> lean, thin, slende,r skinny
- [ ] **listless**  冷漠的；不留心的 <kbd>sym</kbd> heedless, thoughtless, careless<br/>
_He made a **listless** mistake in answering the question._
- [ ] **malignant**  惡毒的 <kbd>sym</kbd> malign, malicious, bitter, spiteful
- [ ] **manful**  剛毅的；勇敢的 <kbd>sym</kbd> bold, courageous, brave, strong, daring
- [ ] **mediocre**  平庸的 <kbd>sym</kbd> ordinary, middling, mean, medium<br/>
_He is a person of **mediocre** abilities._
- [ ] **menial**  低賤的 <kbd>sym</kbd> mean, low, servile<br/>
_She hated such **menial** tasks as washing the pots and pans._
- [ ] **mincing**  矯飾的 <kbd>sym</kbd> affected, over-nice
- [ ] **minuscule**  小寫的；小的 <kbd>sym</kbd> lower-case, small, tiny<br/>
_The **minuscule** investment paid astronomical dividends._
- [ ] **morbid**  不健康的 <kbd>sym</kbd> sickly, diseased, unsound<br/>
_A liking for horrors is **morbid**._
- [ ] **moribund**  將死的 <kbd>sym</kbd> dying, on one's deathbed, with one foot in the grave<br/>
_In the **moribund** patient deepening coma are the usual preludes to death._
- [ ] **naval**  海軍的 <kbd>sym</kbd> marine, nautical, maritime
- [ ] **nocturnal**  夜間的 <kbd>sym</kbd> night <kbd>ant.</kbd> diurnal<br/>
_The owl is a **nocturnal** bird._
- [ ] **null**  無效的 <kbd>sym</kbd> void, useless, invalid, ineffectual<br/>
_A promise obtained by force is legally **null**._
- [ ] **numb**  麻木的 <kbd>sym</kbd> torpid, paralyzed, dulled, insensible<br/>
_My finger are **numb** with cold._
- [ ] **palatable**  味美的 <kbd>sym</kbd> savory, agreeable, enjoyable, favorous<br/>
_Mother provides **palatable** meals for our family every day._
- [ ] **permissible**  可容許的 <kbd>sym</kbd> admissible, allowable<br/>
_Talking in class is not **permissible**._
- [ ] **phlegmatic**  冷漠的；遲鈍的 <kbd>sym</kbd> apathtic, stoical, dull, calm, inert
- [ ] **placid**  安靜的 <kbd>sym</kbd> calm, quiet, tranquil, unmoved
- [ ] **profitless**  無益的 <kbd>sym</kbd> worthless, useless, fruitless, bootless
- [ ] **renowned**  著名的 <kbd>sym</kbd> distinguished, famous, famed, noticeable
- [ ] **ruinous**  毁壞的；荒廢的 <kbd>sym</kbd> destrutive, subversive, wasteful
- [ ] **rustic**  生锈的；荒疏的 <kbd>sym</kbd> musty, rubiginous, rough, dull<br/>
_I'm afraid my German is very **rusty**._
- [ ] **sagacious**  睿智的 <kbd>sym</kbd> discerning, intelligent, rational, sage, shrewd
- [ ] **salutary**  有益健康的 <kbd>sym</kbd> healthy, salubrious, wholesome, helpful, safe<br/>
_Walking is a **salutary** exercise._
- [ ] **sturdy**  不屈的；頑強的 <kbd>sym</kbd> bold, obstinate, trim, stiff, stubborn
- [ ] **sluggish**  怠惰的 <kbd>sym</kbd> idle, indolent, lazy, inactive, drowsy, inert<br/>
_He has a **sluggish** mind and show little interest in anything._
- [ ] **socialbe**  好交際的 <kbd>sym</kbd> compassionable, social, conversable, communicative, genial<br/>
_The Smiths are a **sociable** family._
- [ ] **stray**  漂泊的；偶然的 <kbd>sym</kbd> ramble, rove, wandering, strange<br/>
_The streets were empty except for a few **stray** taxis._
- [ ] **sulky**  溫怒的 <kbd>sym</kbd> cross, morose, sour, sullen, splenetic<br/>
_Spoiled children become **sulky** if they cannot have their own way._
- [ ] **sultry**  悶熱的 <kbd>sym</kbd> warm and damp, humid
- [ ] **superfluous**  過多的；不必要的 <kbd>sym</kbd> excessive, redundant, useless<br/>
_In writing telegrams, omit **superfluous** words._
- [ ] **tentative**  暫時的；試驗性的 <kbd>sym</kbd> temporary, trying, experimenting<br/>
_This is only a **tentative** value._
- [ ] **therapeutic**  治療的 <kbd>sym</kbd> curative, medical, remedial<br/>
_Heat has **therapeutic**._
- [ ] **transitory**  短暫的 <kbd>sym</kbd> passing, temporary, transient, flitting<br/>
_We hope this hot weather will be **transitory**._
- [ ] **tumid**  腫脹的 <kbd>sym</kbd> swelled, swollen, distended
- [ ] **uncouth**  粗魯的；笨拙的 <kbd>sym</kbd> rustic, boorish, awkward