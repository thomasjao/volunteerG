### VOCABULARY

> clever mommy subject degree passionate totally grandchild grandchildren
>
> passionately university ha-ha professor wedding lighter sense of humor
>
> magnificent shouldn't

### EXERCISE 1A

1. A **professor** is someone who teaches at a university.
2. David must be very __________ because he always does very well in his exams.
3. When David was at school, English was his favorite ____________.
4. These days it is useful to have a ____________ degree if you want to get a good job.
5. In your country, how many teenagers study at the __________ after they leave school?

### EXERCISE 1B
*Read the clues and complete the crossword. All the words are in the vocabulary list above.*

1. Very loving and full of emotion.
2. The child of your son or daughter
3. Many children call their mother by this name
4. Another word for completely, entirely
5. Another word for wonderful, beautiful, impressive
6. The ceremony when two people get married

## GRAMMAR
### EXERCISE 2A - should / shouldn't
* You **should** study more.
* David **shouldn't** smoke so much.
* **Should** I book tickets for the movies?
* Don't make these mistakes!

* ~~He shoulds be more careful.~~ (He **should** be more careful.)
* ~~Do I should go now?~~ (**Should** I go now?)
* ~~They don't should be here.~~ (They **shouldn't** be here.)
* ~~I should to take more exercise.~~ (I **should** take more exercise.)

* Use **should** to express obligation:  I **should** spend more time with my family.
* Use **should** to give advice:         You **should** send more time with your family.
* Use **really** to add emphasis:   You really **should** spend more time with your family.
* **Should** is a modal verb.

### EXERCISE 2A - continued
Fill the spaces. Use should / shouldn't plus a verb from the box. There are more verbs than you need!

show be spend let eat find like say go give

1. David really **shouldn't** so many sweets. It's not good for his health.
2. David ____________ another job because he's very clever and he's just bored at the school.
3. He ___________ so much time with his students because it's not professional.
4. _________ we __________ David marry our daughter?
5. We ____________ David any money.
6. David ____________ us how much he loves Juanita.

### EXERCISE 2B - Tell + object + how / where / when /what + to + infinitive

* Could you **tell** me **how to** get to Houston Street?
* Can you **tell** my mother **where to** find some nice gifts?
* Please **tell** us **when 薈鈑罐伙莑徣.
* **Tell** me **what to** say.
* Don't **tell** David **what to** do.
* I told him to tidy his room.

Rewrite the sentences using ***tell*** **+ object + ***how / what / where / when + to***  诮.

**For examples, see the box above.**

1. How do I get to the city center?<br />
   Could you tell ________________________________?
2. How can we find this address?<br />
   Can you tell __________________________________?
3. Where do we buy a ticket for the subway?<br />
   Could you tell ________________________________?
4. What should we see in Washington, D.C.?
   Tell _________________________________________.
5. How should I pronounce this word?<br />
   Please tell __________________________________.
6. When do we get off this train?<br />
   Could you tell _______________________________.
7. How much should we pay for good theatre tickets in New York?<br />
   Tell _________________________________________.
8. Sally, could you answer the telphone?<br />
   I told _______________________________________.
9. Anthony, would you shut the door?<br />
   I told _______________________________________.
   
### EXERCISE 2C - be + so + adjective + that be such a + adjective + noun + that
* Juanita is **such a beautiful** woman **that** I can't take my eyes off her.
* Juanita is **so beautiful that** I can't take my eyes off her.

** New York was such a crazy city that I really enjoyed living there.
* New York was so crazy that I really enjoyed living there.

* so + adverb + that

* You're **so passionately in love with her that** nothing else in the world matters to you.
* They were shouting ** so loudly that** I could hear them from across the street.

**NOTE:** In these sentences, *that* is optional.

### EXERCISE 2C - continued
**Complete the sentences. Use the phrases in the box with "so ... that" or "such a ... that"..**

> interested in David's father  | proud of David | happy
>
> special to us | a rich man | often
>
> well | handsome that | afraid of Mr. Ramirez | a good teacher

1. Juanite is **so special to us that** we want her to have a magnificent wedding.
2. David is _________________ he'll soon get a really good job.
3. We're ________________ we want ot celebrate.
4. Mr. Ramirez is _______________ he can pay for the best wedding possible.
5. Juanita is _______________ she wants her parents to like him.
6. David is ___________________he can't look him in the eye when he speaks.
7. Mrs. Ramirez thinks David is _____________ she wants him to have lots of children.
8. Mr. Ramirez is ____________________ he wants to invite him for lunch very soon.
9. Heinrich is Austrian, but he speaks English ____________ most people think he's American.
10. Angela goes to that shop ____________ they've given her a special discount.

### FUNCTIONS
#### Asking for directions

* Excuse me, which train do I get to go to Houston Street?
* Can you just tell me how to get there?
* Take the E train, and change trains at Columbus Circle.

**Pronunciation**
* You pronounced it incorrectly.
* It's pronounced Houston.
* I don't care how you say it.
* That's how you say it.
* Repeat after me: Houston.

**Money**

* You just want to get your hands on my money, don't you?
* You're just after her money, aren't you?
* You'll just live off my money.

### EXERCISE 3A
**Choose an answer for each question from the box.**
<div style="border: 1px solid #999">
<p>Yes, it does.</p>
<p>Turn left and you'll see it on your right.</p>
<p>Take the E train and change trains at Columbus Circle.</p>
<p>I'm sorry, I didn't understand you.</p>
<p>Of course, what can I do?</p>
</div>

1. A: Excuse me, which train should I get to go to Houston Street?<br />
   B: **I'm sorry, I didn't understand you.**
2. A: Can you tell me how to get to Houston Street?<br />
   B: ____________________________________________.
3. A: Where's the nearest station?<br />
   B: ____________________________________________.
4. A: Does this train go to Columbus Circle?<br />
   B: ____________________________________________.
5. A: Excuse me, can you help me please?<br />
   B: ____________________________________________.
   
### EXERCISE 3B - Pronunciation - sentence stress
In spoken English some words are louder and sound more important than others.

These stressed words are usually the nouns, verbs, and other words which give the meaning.

The other "grammar" words (prepositions, articles, auxiliaries) are usually said quickly and are not stressed.

* I heard that **David Peters** is going to **marry Juanita**.
* I think **he's** a **very lucky man**.
* **Juanita's parents** are **very excited** about the **wedding**, aren't they?

**NOTE**: There are no simple rules. For example,
**I** think it's **good**. and I **think** it's **good**.

are both possible (but have different meanings).

### EXERCISE 3B - continued
**Underline the words you think are most likely to be stressed.**

1. You pronounced it incorrectly.
2. I don't care how you say it.
3. Excuse me, where's the nearest station?
4. Could you tell me how to get to Houston Street, please?
5. I'm sorry, but I really can't help you.
6. Take a taxi to the bus station, and then get a number 47 bus.
7. Go as far as the bank, trun right, and you'll see it.
8. You just want to get your hands on my money, don't you?

### EXERCISE 3C
**Mr. Ramirez thinks that David is only interested in Juanita for her money.**

**Put these words into the correct order to make sentences.**

1. you? don't do You me like just<br />
   ____________________________________
2. sure not you. just We're about<br />
   ____________________________________
3. men Juanita difficult just can know how doesn't be.<br />
   ___________________________________________________
4. daughter. just seem You love don't our to<br />
   _______________________________________________
5. Juanita. what's just for We our want best
   _________________________________________
6. stay you? just all going day, home You're aren't at to<br />
   ____________________________________________________________
7. just my You'll won't live you? money, off<br />
   _______________________________________________
8. how You just remember you should are! lucky<br />
   _________________________________________________
   
### WRAP-UP
**Mr. Ramirez is worried about David. Fill in the gaps with suitable words.**

** What do we know about David?**

On the positive side, Juanita loves him very much! I suppose he's quite clever as he's a (1) ______________.
He teaches English at a private language school. He went to the university and has a (2) ___________
in French and German. Juanita wants him to find a better job because she think he could do better but
perhaps he's a little lazy. I'm not sure yet!

He was very nervous when we met him for the first time, but Conchita immediately liked him and I could
see that she was already planning the (3) __________. I know she wants to have lots of (4) _______________
I'm a business man so I'm usually more careful with people. We have a lot of (5) __________ so I want
to be sure that David isn't just interested in our daughter because we're rich. I'll watch him carefully over the next few months.

The next step will be to meet his father and his sister and then we'll have a better idea!

**Now write some notes about someone you know, using the guidelines below:**

* What does he/she do?
* What did he/she study?
* What did you think when you first met this person? And why?
* What do you think of him/her now?