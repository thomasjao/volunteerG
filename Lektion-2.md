### 1. Suchen Sie W&ouml;rter.

a)tielektroherdwestuhlertopfelemineuaskameratewasserhahnefgl&uuml;hbirneh<br />
________________________________________________________________________

b) zahkugelschreiberledlampesbwaschbecken&ouml;rststecketlobatterieps&uuml;zahlend<br />
________________________________________________________________________

c) tassteckdoseautaschenlampeehtischisfotokistaschenrechnerlas<br />
________________________________________________________________________

### 2. &bdquo;Der&rdquo;, &bdquo;die&rdquo; order &bdquo;das&rdquo;?

* a) _____________ Taschenrechner
* b) _____________ Lampe
* c) _____________ Topf
* d) _____________ Steckdose
* e) _____________ Wasserhahn
* f) _____________ Kugelschreiber
* g) _____________ Elektroherd
* h) _____________ Foto
* i) _____________ Mine
* j) _____________ Gl&uuml;hbirne
* k) _____________ Kamera
* l) _____________ Taschenlampe
* m) _____________ Tisch
* n) _____________ Stuhl
* o) _____________ Waschbecken
* p) _____________ Stecker

### 3. Bildw&ouml;rterbuch. Erg&auml;nzen Sie.

* a) der ________________
* b) ____________________
* c) ____________________
* d) ____________________
* e) ____________________
* f) ____________________
* g) ____________________
* h) ____________________
* i) ____________________
* j) ____________________
* k) ____________________
* l) ____________________
* m) ____________________
* n) ____________________

### 4. &bdquo;Er&rdquo;, &bdquo;sie&rdquo;, &bdquo;es&rdquo; order &bdquo;sie&rdquo; (Plural)? Erg&auml;nzen Sie.

* a) Das ist eine *Leice*. Sie ist schon zwanzig Jahre alt, aber _____ fotografiert noch sehr gut.
* b) Das ist Karins Kugelschreiber. ______ schreibt sehr gut.
* c) Das ist der Reiseleiter. ______ wohnt in Ulm.
* d) Frau Benz ist nicht berufst&auml;tig. ______ ist Hausfrau.
* e) Das sind Inge und Karin. ________ sind noch Sch&uuml;lerinnen.
* f) Das ist Bernds Auto. ________ ist zehn Jahre alt.
* g) Das sind Batterien. ________ sind f&uuml;r Kameras oder Taschenrechner.
* h) Das ist eine GORA-Sp&uuml;lmaschine. Die Maschine hat f&uuml;nf Programme. ________ ist sehr gut.
* i) Das ist ein BADENIA-K&uuml;chenstuhl. Der Stuhl ist sehr bequem. ________ kostet 285 Mark.

### 5. &bdquo;Der&rdquo; order &bdquo;ein&rdquo;, &bdquo;die&rdquo; order &bdquo;eine&rdquo;, &bdquo;das&rdquo; oder &bdquo;ein&rdquo;, &bdquo;die&rdquo; (Plural) oder &bdquo-&rdquo;?

* a) Nr. 6     ist ________ B&uuml;roregal und kostet 136 Nark.
* b) ________  K&uuml;chenregal kostet 180 Mark.
* c) Nr. 8     ist ________ Sp&uuml;le mit zwei Becken.
* d) ________  Sp&uuml;le mit zwei Becken kostet 810 Mark.
* e) ________  Herd Nr. 3 ist ________ Elektroherd, Nr.2 ist ________ Gasherd.
* f) ________  Elektroherd kostet 1280 Mark, ________ Gasherd 935.
* g) ________  Lampen Nr. 10 und 11 sind ________ K&uuml;chenlampen. ________ Lampe Nr. 9 ist ________ B&uuml;rolampe.
* h) ________  K&uump;chenlampen kosten 89 und 126 Mark, ________ B&uuml;rolampe 160.

### 6. Beschreiben Sie.

* a) Das ist ein K&uuml;chenschrank. Der Schrank hat acht Schubladern. Er kostet DM 998.-
* b) Das ist ____________________________________________________________________________
* c) ____________________________________________________________________________________
* d) ____________________________________________________________________________________
* e) ____________________________________________________________________________________
* f) ____________________________________________________________________________________
* g) ____________________________________________________________________________________
* h) ____________________________________________________________________________________
* i) ____________________________________________________________________________________

### 7. Ein Wort pa&szlig;t nicht.

* a) Geschirrsp&uuml;ler - Waschmaschine - Sp&uuml;le - Mikrowelle
* b) Bild - Stuhl - Tisch - Schrank
* c) Sp&uuml;le - Abfalleimer - Waschbecken - Wasserhahn
* d) Elektroherd - K&uuml;hlschrank - Regal - Geschirrsp&uuml;ler
* e) Radio - Telefon - Fernsehapparat - Uhr

### 8. Was ist das

### 9. &bdquo;Wer&rdquo; oder &bdquo;was&rdquo;? Fragen Sie.

* a) Wer ist das?              - Herr Roberts.
* b) _________________________ - Ein Stuhl.
* c) _________________________ - Das ist eine Lampe.
* d) _________________________ - Das ist Margot Schulz.
* e) _______ ist Klaus Henkel? - Programmierer.
* f) __________ ist Studentin? - Monika Sager.
* g) _______ wohnt in Hamburg? - Angelika Wiechert.
* h) ________ macht Rita Kurz? - Sie ist Sekret&auml;rin.

### 10. Was ist da nicht?

* a) Das ist kein ____________.
* b) _________________________.
* c) _________________________.
* d) _________________________.
* e) _________________________.
* f) _________________________.

### 11. Ordnen Sie.

> Elektroherd Taschenlampe Mine Lampe Gl&uuml;hbirne Foto Uhr Radio Fernsehapparat
> Abfalleimer Regal Bild K&uuml;hlschrank Schrank Kugelschreiber Stecker Stuhl
> Steckdose Taschenrechner Sp&uuml;le Geschirrsp&uuml;ler Tisch Mikrowelle
> filled into der/ein/kein, die/eine/keine, das/ein/kein

### 12. Wie hei&szlig;t der Singular? Wie hei&szlig;t der Plural? Erg&auml;nzen Sie.

> Telefon Stuhl Abfalleimer Frau Gl&uuml;hbirne Batterie Hobby Mikrowelle Lampe Foto
> Uhr Mutter Kamera Beruf Stecker Wasserhahn Sp&uuml;lmaschine Regal Kind Mine
> Elektroherd Kochfeld Bild Zahl Name Waschbecken M&auml;dchen Taschenrechner
> Kugelschreiber Tisch Topf Ausl&auml;nder Land Radio Auto Fernsehapparat

`-e` `-` `"-e` `-n` `-en` `"-` `-er` `"-er` `s`

### 13. Schreiben Sie die Zahlen.

* a) zweihundertvierundsechzig
* b) hundertzweiundneunzig
* c) f&uuml;nfhunderteinundachtzig
* d) siebenhundertzw&ouml;lf
* e) sechshundertf&uuml;nfundf&uuml;nfzig
* f) neunhundertdreiundsechzig
* g) hundertachtund zwanzig
* h) dreihundertdreizehn
* i) siebenhunderteinunddrei&szlig;ig
* j) f&uuml;nfhundertsiebenundvierzig
* k) achthundertsechsundachtzig
* l) sechshundertf&uuml;nfundsiebzig
* m) zweihundertachtunddrei&szlig;ig
* n) vierhundertdreiundneunzig
* o) neunhundertzweiundzwanzig
* p) hundertneun
* q) achthundertsechzehn
* r) zweihunderteins

### 14. Schreiben Sie die Zahlen und lesen Sie laut.

* a) 802: ________________________
* b) 109: ________________________
* c) 234: ________________________
* d) 356: ________________________
* e) 788: ________________________
* f) 373: ________________________
* g) 912: ________________________
* h) 401: ________________________
* i) 692: ________________________
* j) 543: ________________________
* k) 428: ________________________
* l) 779: ________________________
* m) 284: ________________________
* n) 997: ________________________
* o) 238: ________________________
* p) 513: ________________________
* q) 954: ________________________
* r) 786: ________________________

### 15. &bdquo;Er&rdquo;, &bdquo;sie&rdquo;, &bdquo;es&rdquo; oder &bdquo;sie&rdquo; (Plural)? Erg&auml;nzen Sie.

* a) &cir; Ist das deine Kamera?  &squ; Ja, aber _______ funktioniert nicht.
* b) &cir; Ist das Ihr Auto? &squ; Ja, aber ________ f&auml;hrt nicht.
* c) &cir; Ist das deine Taschenlampe? &squ; Ja, aber ________ funktioniert nicht.
* d) &cir; Ist das dein Taschenrechner? &squ; Ja, aber ________ geht nicht.
* e) &cir; Sind das Ihre Batterien? &squ; Ja, aber ________ sind leer.
* f) &cir; Ist das Ihre Uhr? &squ; Ja, aber ________ geht nicht.
* g) &cir; Sing das Ihre Kugelschreiber? &squ; Ja, aber ________ schreiben nicht.
* h) &cir; Ist das dein Telefon? &squ; Ja, aber ________ geht nicht.

### 16. &bdquo;Ihr&rdquo;/&bdquoIhre&rdquo; oder &bdquo;dein&rdquo;/&bdquo;deine&rdquo;? Erg&auml;nzen Sie.

* a) &cir; Entschuldigen Sie! Ist das ________ Uhr? &squ; Ja.
* b) &cir; Du Sonja, ist das ________ Auto? &squ; Nein.
* c) &cir; Frau Kunst, wie ist ________ Telefonnummer? &squ; 24 56 89.
* d) &cir; Wie ist ________ Addresse, Herr Wenzel? &squ; Konradstra&szlig;e 35, 6500 Mainz.
* e) &cir; Wie hei&szlig;t du? &squ; Bettina.<br />
  &nbsp; &cir; Und was ist ________ Addresse? &squ; Mozartstra&szlig; 23.
* f) &cir; Hast du jetzt Telefon? &squ; Ja.<br />
  &nbsp; &cir; Und wie ist ________ Nummer? &squ; 5 78 54.
     
### 17. Erg&auml;nzen Sie.

* a) Taschenlampe : Batterie / Auto : ___________
* b) Fernsehapparat : Bild / Kamera : ___________
* c) Batterie : leer / Stuhl : ________
* d) Sp&uuml;lmaschine : sp&uuml;len / Waschmaschine : ____________
* e) Postkarte : lesen und schreiben / Telefon : ________________ und ____________
* f) Auto : waschen / Topf : ____________
* g) Mikrowelle : praktisch / Stuhl : ____________

### 18. Was pa&szlig; nicht?

* a) *Die Waschmaschine:* ist praktisch, ist gut, ist neu, f&auml;hrt gut, w&auml;scht gut.
* b) *Das Huas:* ist klein, ist modern, ist ehrlich, kostet DM 430 000.
* c) *Der K&uuml;hlschrank:* ist leer, geht nicht, sp&uuml;lt nicht, ist praktisch, ist neu.
* d) *Das Telefon:* ist lustig, antwortet nicht, ist kaputt, ist modern.
* e) *Die Frau:* ist kaputt, ist ehrlich, ist ledig, ist klein, ist lustig.
* f) *Die Sp&uuml;lmaschine:* w&auml;scht nicht, ist leer, geht nicht, sp&uuml;lt nicht gut.
* g) *Der Stuhl:* ist bequem, ist neu, ist leer, ist frei, ist modern.
* h) *Das Foto:* ist lustig, ist praktisch, ist neu, ist klein, ist gut.
* i) *Das Auto:* f&auml;hrt nicht, ist neu, w&auml;scht gut, ist kaputt, ist gut.
* j) *Das Gesch&auml;ft:* ist gut, ist neu, ist klein, ist leer, ist ledig.
* k) *Die Idee:* ist neu, ist lustig, ist klein, ist gut.
* l) *Die K&uuml;che:* ist modern, ist ehrlich, ist praktisch, ist neu, ist klein.

### 19. Antworten Sie.

a) &cir; Ist das deine Uhr?<br />
   &squ; Nein, das ist ihre Uhr.
   
b) &cir; Sind das deine Fotos?<br />
   &squ; _________________________________
   
c) &cir; Ist das dein Kugelschreiber?<br />
   &squ; _________________________________
   
d) &cir; Ist das dein Radio?<br />
   &squ; _________________________________
   
e) &cir; Ist das deine Lampe?<br />
   &squ; _________________________________
   
f) &cir; Ist das dein Fernsehapparat?<br />
   &squ; _________________________________
   
g) &cir; Sind das deine Batterien?<br />
   &squ; _________________________________
   
h) &cir Ist das deine Kamera?<br />
   &squ; _________________________________
   
i) &cir; Ist das dein Auto?<br />
   &squ; _________________________________
   
j) &cir Ist das dein Taschenrechner?<br />
   &squ; _________________________________